#!/bin/bash

git submodule update --init frobby

#compile frobby
cd frobby
#throws error as it cannot copy library file into /bin, but we can ignore it!
make -j4 library GMP_INC_DIR=$(realpath ../.gmp_lib/include) ldflags_for_gmp="-L$(realpath ../.gmp_lib/lib)" CFLAGS="-m64 -mtune=core2 -march=core2" LDFLAGS=$CFLAGS
cd ..
