#!/bin/bash

/bin/rm -r gfanlib
wget -O gfanlib.tar.gz https://math.au.dk/~jensen/software/gfan/gfanlib0.6.2.tar.gz
tar -xf gfanlib.tar.gz
/bin/rm gfanlib.tar.gz
