#!/bin/bash

#clean all libs

cd CoCoALib
make distclean
cd ..

./download_cocoalib.sh

./download_gfanlib.sh

./download_gmp.sh

./download_mathsat.sh

cd boost
./b2 --clean
cd ..

cd cddlib
make distclean
cd ..

cd readline
make distclean
cd ..

cd frobby
make clean
cd ..

cd gsl
make distclean
cd ..

cd Normaliz
make distclean
cd ..

cd ApCoCoALib
make distclean
cd ..


#remove readline and cddlib header-file which where moved by
/bin/rm -r .cdd_lib
/bin/rm -r .gsl_lib
/bin/rm -r .gmp_lib
/bin/rm -r .boost_lib
/bin/rm -r .readline_lib
/bin/rm -r .normaliz_lib
/bin/rm -r .cocoa_lib
/bin/rm -r .cocoa5
/bin/rm -r .apcocoa
/bin/rm -r .apcocoa_server


