#!/bin/bash

#compile gmp
cd gmp
./configure --enable-cxx --prefix=$(realpath ../.gmp_lib) --exec-prefix=$(realpath ../.gmp_lib)
make clean

make -j4
make install -j4
cd ..
