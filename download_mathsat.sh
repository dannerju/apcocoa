#!/bin/bash

/bin/rm -r MathSAT
wget -O mathsat.tar.gz https://mathsat.fbk.eu/download.php?file=mathsat-5.6.6-linux-x86_64.tar.gz
tar -xf mathsat.tar.gz
/bin/rm mathsat.tar.gz
mv mathsat* MathSAT
