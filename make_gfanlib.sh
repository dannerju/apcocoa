#!/bin/bash

#compile gfanlib
cd gfanlib
./configure CPPFLAGS="-I$(realpath ../.cdd_lib/include/cdd) -I$(realpath ../.cdd_lib/include) -I$(realpath ../.gmp_lib/include) -L$(realpath ../.gmp_lib/lib)"
make -j4 libgfan.a CPPFLAGS="-I$(realpath ../.cdd_lib/include/cdd) -I$(realpath ../.cdd_lib/include) -I$(realpath ../.gmp_lib/include) -L$(realpath ../.gmp_lib/lib)"
cd ..
