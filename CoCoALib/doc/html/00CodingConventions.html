<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>      Coding Conventions</title>
<meta name="generator" content="https://txt2tags.org">
<link rel="stylesheet" href="cocoalib-doc.css">
<style type="text/css">
blockquote{margin: 1em 2em; border-left: 2px solid #999;
  font-style: oblique; padding-left: 1em;}
blockquote:first-letter{margin: .2em .1em .1em 0; font-size: 160%; font-weight: bold;}
blockquote:first-line{font-weight: bold;}
body{font-family: sans-serif;}
hr{background-color:#000;border:0;color:#000;}
hr.heavy{height:2px;}
hr.light{height:1px;}
img{border:0;display:block;}
img.right{margin:0 0 0 auto;}
img.center{border:0;margin:0 auto;}
table{border-collapse: collapse;}
table th,table td{padding: 3px 7px 2px 7px;}
table th{background-color: lightgrey;}
table.center{margin-left:auto; margin-right:auto;}
.center{text-align:center;}
.right{text-align:right;}
.left{text-align:left;}
.tableborder,.tableborder td,.tableborder th{border:1px solid #000;}
.underline{text-decoration:underline;}
</style>
</head>
<body>
<header>
<hgroup>
<h1>      Coding Conventions</h1>
<h2>      &copy;  2007-2010  John Abbott,  Anna M. Bigatti</h2>
<h3>      GNU Free Documentation License, Version 1.2</h3>
</hgroup>
</header>
<article>

<nav>
<div class="body" id="body">

    <ul>
    <li><a href="#toc1">User and contributor documentation</a>
      <ul>
      <li><a href="#toc2">Names of CoCoA types, functions, variables</a>
      </li>
      <li><a href="#toc3">Order in function arguments</a>
        <ul>
        <li><a href="#toc4">Explanation notes, exceptions, and more examples</a>
        </li>
        </ul>
      </li>
      <li><a href="#toc5">Abbreviations</a>
      </li>
      </ul>
    </li>
    <li><a href="#toc6">Contributor documentation</a>
      <ul>
      <li><a href="#toc7">Guidelines from Alexandrescu and Sutter</a>
      </li>
      <li><a href="#toc8">Use of #define directive</a>
      </li>
      <li><a href="#toc9">Header Files</a>
      </li>
      <li><a href="#toc10">Curly brackets and indentation</a>
      </li>
      <li><a href="#toc11">Inline Functions</a>
      </li>
      <li><a href="#toc12">Exception Safety</a>
      </li>
      <li><a href="#toc13">Dumb/Raw Pointers</a>
      </li>
      <li><a href="#toc14">Preprocessor Symbols for Controlling Debugging</a>
      </li>
      <li><a href="#toc15">Errors and Exceptions</a>
        <ul>
        <li><a href="#toc16">During development</a>
        </li>
        <li><a href="#toc17">Always</a>
        </li>
        </ul>
      </li>
      <li><a href="#toc18">Functions Returning Complex Values</a>
      </li>
      <li><a href="#toc19">Spacing and Operators</a>
      </li>
      </ul>
    </li>
    </ul>

</div>
</nav>
<div class="body" id="body">
<p>
      <center><a href="index.html">CoCoALib Documentation Index</a></center>
</p>

<section~A~>
<h1></h1>
<section id="toc1">
<h2>User and contributor documentation</h2>

<p>
This page summarises the coding conventions used in CoCoALib.  This
document is useful primarily to contributors, but some users may find
it handy too.  As the name suggests, these are merely guidelines; they
are not hard and fast rules.  Nevertheless, you should violate these
guidelines only if you have genuinely good cause.  We would also be
happy to receive notification about parts of CoCoALib which do not
adhere to the guidelines.
</p>
<p>
We expect these guidelines to evolve slowly with time as experience grows.
</p>
<p>
Before presenting the guidelines I mention some useful books.
The first is practically a <em>sine qua non</em> for the C++ library:
<strong>The C++ Standard Library</strong> by Josuttis which contains essential
documentation for the C++ library.  Unless you already have quite a
lot of experience in C++, you should read the excellent books by Scott
Meyers: <strong>Effective C++</strong> (the new version), and <strong>Effective STL</strong>.
Another book offering useful guidance is <strong>C++ Coding Standards</strong> by
Alexandrescu and Sutter; it is a good starting point for setting
coding standards.
</p>

<section id="toc2">
<h3>Names of CoCoA types, functions, variables</h3>

<p>
All code and "global" variables must be inside the namespace <code>CoCoA</code>
(or in an anonymous namespace); the only exception is code which is
not regarded as an integral part of CoCoA (e.g. the C++ interface to
the GMP big integer package).
</p>
<p>
There are numerous conventions for how to name classes/types,
functions, variables, and other identifiers appearing in a large
package.  It is important to establish a convention and apply it
rigorously (plus some common sense); doing so will facilitate
maintenance, development and use of the code.
(The first three rules follow the convention implicit in <strong>NTL</strong>)
</p>

<ul>
<li>single word names are all lower-case (<em>e.g.</em> <code>ring</code>);
</li>
<li>multiple word names have the first letter of each word capitalized,
  and the words are juxtaposed (rather than separated by underscore,
   (<em>e.g.</em> <code>PolyRing</code>);
</li>
<li>acronyms should be all upper-case (<em>e.g.</em> <code>PPM</code>);
</li>
<li>names of functions returning a boolean start with <code>Is</code>
  (<code>Are</code> if argument is a list/vector);
</li>
<li>names of functions returning a <a href="bool3.html"><code>bool3</code></a> start with <code>Is</code> and end with <code>3</code>
  (<code>Are</code> if argument is a list/vector);
</li>
<li>variables of type (or functions returning a) pointer end with <code>Ptr</code>
</li>
<li>data members' names start with <code>my</code> (or <code>Iam/Ihave</code> if they are boolean);
</li>
<li>a class static member has a name beginning with <code>our</code>;
</li>
<li>enums are called <code>BlahMarker</code> if they have a single value
  (<em>e.g.</em> <code>BigInt::CopyFromMPZMarker</code>) and <code>BlahFlag</code> if they have more;
</li>
<li>abbreviations should be used consistently (see below);
</li>
<li><strong>Abstract base classes</strong> and <strong>derived abstract classes</strong> normally
  have names ending in <code>Base</code>;
  in contrast, a <strong>derived concrete class</strong> normally has a name ending
  in <code>Impl</code>.  Constructors for abstract classes should probably be
  <code>protected</code> rather than <code>public</code>.
</li>
</ul>

<p>
It is best to choose a name for your function which differs from the
names of functions in the C++ standard library, otherwise it can become
necessary to use fully qualified names (e.g. <code>std::set</code> and
<code>CoCoA::set</code>) which is terribly tedious.
(Personally, I think this is a C++ design fault)
</p>
<p>
If you are overloading a C++ operator then write the keyword
<code>operator</code> attached to the operator symbol (with no intervening
space).  See <code>ring.H</code> for some examples.
</p>

</section>
<section id="toc3">
<h3>Order in function arguments</h3>

<p>
When a function has more than one argument we follow the first
applicable of the following rules:
</p>

<ol>
<li>the non-const references are the first args, e.g.
 <ul>
 <li><code>myAdd(a,b,c)</code> as in <em>a=b+c</em>,
 </li>
 <li><code>IsIndetPosPower(long&amp; index, BigInt&amp; exp, pp)</code>
 </li>
 </ul>
</li>
<li>the ring/PPMonoid is the first arg, e.g.
 <ul>
 <li><code>ideal(ring, vector&lt;RingElem&gt;)</code>
 </li>
 </ul>
</li>
<li>the <em>main actor</em> is the first arg and the <em>with respect to</em> args follow, e.g.
 <ul>
 <li><code>deriv(f, x)</code>
 </li>
 </ul>
</li>
<li>optional args go last, e.g.
 <ul>
 <li><code>NewPolyRing(CoeffRing, NumIndets)</code>,
 </li>
 <li><code>NewPolyRing(CoeffRing, NumIndets, ordering)</code>
 </li>
 </ul>
</li>
<li>the arguments follow the order of the common use or <em>sentence</em>, e.g.
 <ul>
 <li><code>div(a,b)</code> for <em>a/b</em>,
 </li>
 <li><code>IndetPower(P, long i, long/BigInt exp)</code> for <em>x[i]^exp</em>,
 </li>
 <li><code>IsDivisible(a,b)</code> for <em>a is divisible by b</em>,
 </li>
 <li><code>IsContained(a,b)</code> for <em>a is contained in b</em>
 </li>
 </ul>
</li>
<li>strongly related functions behave as if they were overloading
   (--&gt; optional args go last), (??? is this ever used apart from <code>NewMatrixOrdering(long NumIndets, long GradingDim, ConstMatrixView OrderMatrix);</code>???)
</li>
<li>the more structured objects go first, e.g. ... (??? is this ever used ???)
</li>
</ol>

<p>
<strong>IMPORTANT</strong> we are trying to define a <strong>good set of few rules</strong>
which is easy to apply and, above all, respects <em>common sense</em>.
If you meet a function in CoCoALib not following these rules let
us know: we will fix it, or fix the rules, or call it an interesting
exception ;-)
</p>

<section id="toc4">
<h4>Explanation notes, exceptions, and more examples</h4>

<ul>
<li>We don't think we have any function with 1 and 2 colliding
</li>
<li>The <em>main actor</em> is the object which is going to be worked on
  to get the returning value (usually of the same type), the
  <em>with respect to</em> are strictly constant, e.g.
  <ul>
  <li><code>deriv(f, x)</code>
  </li>
  <li><code>NF(poly, ideal)</code>
  </li>
  </ul>
</li>
<li>Rule 1 wins on rule 4, e.g.
  <ul>
  <li><code>IsIndetPosPower(index, exp, pp)</code> and <code>IsIndetPosPower(pp)</code>
  </li>
  </ul>
</li>
<li>Rule 2 wins on rule 4, e.g.
  <ul>
  <li><code>ideal(gens)</code> and <code>ideal(ring, gens)</code>
  </li>
  </ul>
</li>
<li>we should probably change:
  <ul>
  <li><code>NewMatrixOrdering(NumIndets, GradingDim, M)</code> into <code>NewMatrixOrdering(M, GradingDim)</code>
  </li>
  </ul>
</li>
</ul>

</section>
</section>
<section id="toc5">
<h3>Abbreviations</h3>

<p>
The overall idea is that if a given concept in a class or function
name always has the same name: either always the full name, or always
the same abbreviation.  Moreover a given abbreviation should have a
unique meaning.
</p>
<p>
Here is a list for common abbreviations
</p>

<ul>
<li><code>col</code> -- column
</li>
<li><code>ctor</code> -- constructor
</li>
<li><code>deg</code> -- degree (exceptions: <code>degree</code> in class names)
</li>
<li><code>div</code> -- divide
</li>
<li><code>dim</code> -- dimension
</li>
<li><code>elem</code> -- element
</li>
<li><code>mat</code> -- matrix (exceptions: <code>matrix</code> in class names)
</li>
<li><code>mul</code> -- multiply
</li>
<li><code>pos</code> -- positive (or should it be <code>positive</code>?  what about <code>IsPositive(BigInt N)</code>?)
</li>
</ul>

<p>
Here is a list of names that are written in full
</p>

<ul>
<li><code>assign</code>
</li>
<li><code>one</code> -- not <code>1</code>
</li>
<li><code>zero</code> -- not <code>0</code>
</li>
</ul>

</section>
</section>
<section id="toc6">
<h2>Contributor documentation</h2>

<section id="toc7">
<h3>Guidelines from Alexandrescu and Sutter</h3>

<p>
Here I paraphrase some of the suggestions from their book, picking out the
ones I think are less obvious and are most likely to be relevant to CoCoALib.
</p>

<ul>
<li>Write correct, clean and simple code at first; optimize later.
</li>
<li>Keep track of object ownership; document any "unusual" behaviour.
</li>
<li>Keep implementation details hidden (<em>e.g.</em> make data members <code>private</code>)
</li>
<li>Use <code>const</code> as much as you reasonably can.
</li>
<li>Use prefix <code>++</code> and <code>--</code> (unless you specifically do want the postfix behaviour)
</li>
<li>Each class should have a <em>single</em> clearly defined purpose; keep it simple!
</li>
<li>Guideline: member fns should be either <code>virtual</code> or <code>public</code> not both.
</li>
<li>Exception cleanliness: dtors, deallocate and <code>swap</code> should never throw.
</li>
<li>Use <code>explicit</code> to avoid making unintentional "implicit type conversions"
</li>
<li>Avoid <code>using</code> in header files.
</li>
<li>Use <code>CoCoA_THROW_ERROR</code> for sanity checks on args to public fns, and <code>CoCoA_ASSERT</code> for internal fns.
</li>
<li>Use <code>std::vector</code> unless some other container is obviously better.
</li>
<li>Avoid casting; if you must, use a C++ style cast (<em>e.g.</em> <code>static_cast</code>)
</li>
</ul>

</section>
<section id="toc8">
<h3>Use of #define directive</h3>

<p>
Excluding the <em>read once trick</em> for header files, <code>#define</code>
should be avoided (even in experimental code).  C++ is rich enough that
normally there is a cleaner alternative to a <code>#define</code>: for
instance, <code>inline</code> functions, a <code>static const</code>
object, or a <code>typedef</code> -- in any case, one should avoid
polluting the global namespace.
</p>
<p>
If you must define a preprocessor symbol, its name should begin with
the prefix <code>CoCoA_</code>, and the remaining letters should all be
capital.
</p>

</section>
<section id="toc9">
<h3>Header Files</h3>

<p>
The <em>read once trick</em> uses preprocessor symbols starting
with <code>CoCoA_</code> and then finishing with the file
name (retaining the capitalization of the file name but with slashes
replaced by underscores).  The include path passed to the compiler
specifies the directory above the one containing the CoCoALib header files,
so to include one of the CoCoALib header files you must
prefix <code>CoCoA/</code> to the name of the file -- this
avoids problems of ambiguity which could arise if two includable files have
the same name.  This idea was inspired by NTL.
</p>
<p>
Include only the header files you really need -- this is trickier to
determine than you might imagine.  The reasons for minimising includes are
two-fold: to speed compilation, and to indicate to the reader which
external concepts you genuinely need.  In header files it often suffices
simply to write a forward declaration of a class instead of including the
header file defining that class.  In implementation files the definition
you want may already be included indirectly; in such cases it is enough to
write a comment about the indirectly included definitions you will be
using.
</p>
<p>
In header files I add a commented out <code>using</code> command
immediately after including a system header to say which symbols are
actually used in the header file.  In the implementation file I write
a <code>using</code> command for each system symbol used in the file; these
commands appear right after the <code>#include</code> directive which
imported the symbol.
</p>

</section>
<section id="toc10">
<h3>Curly brackets and indentation</h3>

<p>
Sutter claims curly bracket positioning doesn't matter: he's wrong!
Matching curly brackets should be either vertically or horizontally
aligned.  Indentation should be small (<em>e.g.</em> two positions for each
level of nesting); have a look at code already in CoCoALib to see the
preferred style.  Avoid using tabs for indentation as these do not have a
universal interpretation.
</p>
<p>
The <code>else</code> keyword indents the same as its matching
<code>if</code>.
</p>

</section>
<section id="toc11">
<h3>Inline Functions</h3>

<p>
Use <code>inline</code> sparingly.  <code>inline</code> is useful in two
circumstances: for a short function which is called very many times (at
least several million), or for an extremely short function (e.g. a field
accessor in a class).  The first case may make the program faster; the
second may make it shorter.  You can use a profiler
(<em>e.g.</em> <code>gprof</code>) to count how often a function is called.
</p>
<p>
There are two potential disadvantages to <code>inline</code> functions:
they may force implementation details to be publicly visible, and they
may cause code bloat.
</p>

</section>
<section id="toc12">
<h3>Exception Safety</h3>

<p>
<em>Exception Safety</em> is an expression invented/promulgated by Sutter to
mean that a procedure behaves well when an exception is thrown during its
execution.  All the main functions and procedures in CoCoALib should be
fully exception safe: either they complete their computations and return
normally, or they leave all arguments essentially unchanged, and return
exceptionally.  A more relaxed approach is acceptable for
functions/procedures which a normal library user would not call directly
(<em>e.g.</em> non-public member functions): it suffices that no memory is
leaked (or other resources lost).  Code which is not fully exception-safe
should be clearly marked as such.
</p>
<p>
Consult one of Sutter's (irritating) books for more details.
</p>

</section>
<section id="toc13">
<h3>Dumb/Raw Pointers</h3>

<p>
If you're using dumb/raw pointers, improve your design!
</p>
<p>
Dumb/raw pointers should be used only as a last resort; prefer C++ references
or <code>std::auto_ptr&amp;lt;...&amp;gt;</code> if you can.  Note
that it is especially hard writing exception safe code which contains dumb/raw
pointers.
</p>

</section>
<section id="toc14">
<h3>Preprocessor Symbols for Controlling Debugging</h3>

<p>
During development it will be useful to have functions perform
<em>sanity checks</em> on their arguments.  For general use, these checks
could readily produce a significant performance hit.
</p>
<p>
Compilation without setting any preprocessor variables should produce
fast code (i.e. without non-vital checks).  Instead there is a
preprocessor symbol (<code>CoCoA_DEBUG</code>) which can be set to
turn on extra sanity checks.
Currently if <code>CoCoA_DEBUG</code> has value zero, all non-vital checks
are disabled; any non-zero value enables all additional checks.
</p>
<p>
There is a macro <code>CoCoA_ASSERT(...)</code> which will check that its
argument yields <code>true</code> when <code>CoCoA_DEBUG</code> is set;
if <code>CoCoA_DEBUG</code> is not set it does nothing (not even evaluating
its argument).  This macro is useful for conducting extra sanity checks
during debugging; it should <strong>not be used</strong> for checks that must always
be performed (<em>e.g.</em> in the final optimized compilation).
</p>
<p>
There is currently no official preprocessor symbol for (de)activating
the gathering of statistics.
</p>
<p>
<strong>NB</strong> I wish to avoid having a plethora of symbols for switching
debugging on and off in different sections of the code, though I do
accept that we may need more than just one or two symbols.
</p>

</section>
<section id="toc15">
<h3>Errors and Exceptions</h3>

<section id="toc16">
<h4>During development</h4>

<p>
Conditions we want to verify <strong>solely during development</strong> (i.e. when
compiling with <code>-DCoCoA_DEBUG</code>) can be checked simply by using
the macro <code>CoCoA_ASSERT</code> with argument the condition.  Should
the condition be false, a <code>CoCoA::ErrorInfo</code> object is thrown --
this will cause an abort if not caught.  The error message indicates the
file and line number of the failing assertion.  If the compilation
option <code>-DCoCoA_DEBUG</code> is not enabled then the macro does
nothing whatsoever.  An example of its use is:
</p>

<pre>
  CoCoA_ASSERT(index &lt;= 0 &amp;&amp; index &lt; length);
</pre>

</section>
<section id="toc17">
<h4>Always</h4>

<p>
A different mechanism is to be used for conditions which must be
checked even <strong>after development is completed</strong>.
</p>
<p>
What should happen when one tries to divide by zero?  Or even asks for
an exact division between elements that do not have an exact quotient
(in the given ring)?
</p>
<p>
Answer: call the macro <code>CoCoA_THROW_ERROR(err_type, location)</code> where
<code>err_type</code> should be one of the error codes listed in <code>error.H</code>
and <code>location</code> is a string saying where the error was detected
(e.g. the name of the function which discovered it).
Here is an example
</p>

<pre>
  if (trouble)
    CoCoA_THROW_ERROR(ERR::DivByZero, "applying partial ring homomorphism");
</pre>

<p>
The macro <code>CoCoA_THROW_ERROR</code> never returns: it will throw
a <code>CoCoA::ErrorInfo</code> object.  See the example programs for the
recommended way of catching and handling exceptions: so that an informative
message can be printed out.  See <code>error.txt</code> for advice on
debugging when an unexpected CoCoA error is thrown.
</p>

</section>
</section>
<section id="toc18">
<h3>Functions Returning Complex Values</h3>

<p>
C++ tends to copy the return value of a function; this is undesirable if the
value is potentially large and complex.  An obvious alternative is to supply
as argument a reference into which the result will be placed.  If you choose
to return the value via a reference argument then make the reference argument
<strong>the first one</strong>.
</p>

<pre>
  myAdd(rawlhs, rawx, rawy);  // stands for:  lhs = x + y
</pre>

</section>
<section id="toc19">
<h3>Spacing and Operators</h3>

<p>
All <strong>binary operators</strong> should have one space before and one space after
the operator name (unless both arguments are particularly short and simple).
<strong>Unary operators</strong> should not be separated from their arguments by
any spaces. 
Avoid spaces between <strong>function</strong> names and the immediately
following bracket.
</p>

<pre>
expr1 + expr2;
!expr;
UsefulFunction(args);
</pre>

</section>
</section>
</section>
</div>
</article></body></html>
