<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>      ExternalLibs-GMP</title>
<meta name="generator" content="https://txt2tags.org">
<link rel="stylesheet" href="cocoalib-doc.css">
<style type="text/css">
blockquote{margin: 1em 2em; border-left: 2px solid #999;
  font-style: oblique; padding-left: 1em;}
blockquote:first-letter{margin: .2em .1em .1em 0; font-size: 160%; font-weight: bold;}
blockquote:first-line{font-weight: bold;}
body{font-family: sans-serif;}
hr{background-color:#000;border:0;color:#000;}
hr.heavy{height:2px;}
hr.light{height:1px;}
img{border:0;display:block;}
img.right{margin:0 0 0 auto;}
img.center{border:0;margin:0 auto;}
table{border-collapse: collapse;}
table th,table td{padding: 3px 7px 2px 7px;}
table th{background-color: lightgrey;}
table.center{margin-left:auto; margin-right:auto;}
.center{text-align:center;}
.right{text-align:right;}
.left{text-align:left;}
.tableborder,.tableborder td,.tableborder th{border:1px solid #000;}
.underline{text-decoration:underline;}
</style>
</head>
<body>
<header>
<hgroup>
<h1>      ExternalLibs-GMP</h1>
<h2>      &copy;  2016,2022  John Abbott, Anna M. Bigatti</h2>
<h3>      GNU Free Documentation License, Version 1.2</h3>
</hgroup>
</header>
<article>

<nav>
<div class="body" id="body">

    <ul>
    <li><a href="#toc1">User documentation</a>
      <ul>
      <li><a href="#toc2">Examples</a>
      </li>
      <li><a href="#toc3">Installing the GMP library</a>
        <ul>
        <li><a href="#toc4">Download and compile GMP</a>
        </li>
        <li><a href="#toc5">Telling CoCoALib where GMP is</a>
        </li>
        </ul>
      </li>
      </ul>
    </li>
    <li><a href="#toc6">Maintainer documentation</a>
    </li>
    <li><a href="#toc7">Bugs, shortcomings and other ideas</a>
    </li>
    <li><a href="#toc8">Main changes</a>
    </li>
    </ul>

</div>
</nav>
<div class="body" id="body">
<p>
      <center><a href="index.html">CoCoALib Documentation Index</a></center>
</p>

<section~A~>
<h1></h1>
<section id="toc1">
<h2>User documentation</h2>

<p>
<strong>GMP is an essential external library</strong> for CoCoALib: a sufficiently
recent version of GMP (5.1.0 or later) must be present to permit
compilation of CoCoALib.  The CoCoALib classes <code>BigInt</code> and <code>BigRat</code>
are simply wrappers for the underlying GMP types.
</p>
<p>
At the moment CoCoALib does not require the C++ interface to GMP,
however if you wish to combine CoCoALib with the external library
<strong>Normaliz</strong> then the C++ interface to GMP must also be present.
</p>

<section id="toc2">
<h3>Examples</h3>

<ul>
<li><a href="../../examples/index.html#ex-BigInt1.C">ex-BigInt1.C</a>
</li>
<li><a href="../../examples/index.html#ex-BigInt2.C">ex-BigInt2.C</a>
</li>
<li><a href="../../examples/index.html#ex-BigInt3.C">ex-BigInt3.C</a>
</li>
<li><a href="../../examples/index.html#ex-BigRat1.C">ex-BigRat1.C</a>
</li>
<li><a href="../../examples/index.html#ex-GMPAllocator1.C">ex-GMPAllocator1.C</a>
</li>
<li><a href="../../examples/index.html#ex-GMPAllocator2.C">ex-GMPAllocator2.C</a>
</li>
</ul>

</section>
<section id="toc3">
<h3>Installing the GMP library</h3>

<p>
It is common for Linux computers to have the <strong>GMP library already</strong>
<strong>installed;</strong> the CoCoALib <code>configure</code> script will check for this,
and will give an error message if it cannot be found.
</p>
<p>
In the unlikely event that GMP was not found by the CoCoALib
<code>configure</code> script, we recommend using your computer's
package manager to install a package having a name something like
<code>libgmpxx-devel</code> (or maybe just <code>libgmp-devel</code>).  You should
pick the version "for developers" as that is what CoCoALib needs.
</p>

<section id="toc4">
<h4>Download and compile GMP</h4>

<p>
If you cannot install the GMP library using your package manager,
you can try compiling it yourself!
If you have little or no experience compiling software, ask
someone who has experience to help you.  It is not hard, but
experience helps a lot!
</p>
<p>
To build the GMP library yourself, you must first download the sources from
</p>

<table style="margin-left: auto; margin-right: auto;">
<tr>
<td><a href="http://www.gmplib.org/">**GMP** website</a></td>
</tr>
</table>

<p>
Note that <strong>CoCoALib</strong> expects <strong>GMP release 5.1.0 or later.</strong>
</p>
<p>
You must then decide whether to make a "personal" installation or
a "system-wide" installation.  To make a system-wide installation
your account must have "administrator privileges"; in case of
doubt, make a personal installation.
</p>

<pre>
## Commands for a ***system-wide*** installation
tar xzf gmp-6.2.1.tar.gz
cd gmp-6.2.1
./configure  --enable-cxx
make
sudo make install
## Maybe run the ldconfig command to register the shared library.
## You can now delete gmp-6.2.1.tar.gz and the directory gmp-6.2.1
</pre>

<p>
To make a personal installation you must decide where GMP should
be installed.  We suggest something like <code>$HOME/MySoftware</code>
(if you choose somewhere else, change the lines below accordingly).
</p>

<pre>
## Commands for a ***personal*** installation
mkdir "$HOME/MySoftware"
./configure  --enable-cxx  --disable-shared  --prefix="$HOME/MySoftware"
make
make install
## You can now delete gmp-6.2.1.tar.gz and the directory gmp-6.2.1
</pre>

</section>
<section id="toc5">
<h4>Telling CoCoALib where GMP is</h4>

<p>
If you compiled GMP and did a system-wide installation then
the CoCoALib <code>configure</code> script should find it automatically.
</p>
<p>
If you compiled GMP and made a personal installation then you
must tell the CoCoALib <code>configure</code> script where to find GMP
by using the option <code>--with-libgmp</code>.  Here is an example:
</p>

<pre>
cd CoCoALib-0.99  # go into the CoCoALib directory
./configure  --with-libgmp="$HOME/MySoftware/lib/libgmp.a"
</pre>

<p>
<strong>FOOTNOTE</strong>
System-wide installation on a GNU/Linux computer is
<strong>possible only if</strong> your user account has the relevant permissions.
You can check whether your account has permission by running
<code>sudo -v</code>.  This will ask for your password, and give an error
if your account does not have permission.
</p>

</section>
</section>
</section>
<section id="toc6">
<h2>Maintainer documentation</h2>

<p>
A future version of CoCoALib may use the C++ interface to GMP, but
that it not imminent.
</p>

</section>
<section id="toc7">
<h2>Bugs, shortcomings and other ideas</h2>

<p>
Strictly the parts of CoCoALib which compute with small finite fields
do not need the GMP library; nevertheless we chose not to permit
compilation without GMP.
</p>
<p>
I found the following website useful:
</p>

<pre>
http://tldp.org/HOWTO/Program-Library-HOWTO/shared-libraries.html
</pre>

</section>
<section id="toc8">
<h2>Main changes</h2>

<p>
<strong>2022</strong>
</p>

<ul>
<li>February: guidelines system-wide/personal installation (now necessary for CoCoALib)
</li>
</ul>

<p>
<strong>2016</strong>
</p>

<ul>
<li>10 June: first version; added new note about ldconfig
</li>
</ul>

</section>
</section>
</div>
</article></body></html>
