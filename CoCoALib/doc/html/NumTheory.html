<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>      NumTheory</title>
<meta name="generator" content="https://txt2tags.org">
<link rel="stylesheet" href="cocoalib-doc.css">
<style type="text/css">
blockquote{margin: 1em 2em; border-left: 2px solid #999;
  font-style: oblique; padding-left: 1em;}
blockquote:first-letter{margin: .2em .1em .1em 0; font-size: 160%; font-weight: bold;}
blockquote:first-line{font-weight: bold;}
body{font-family: sans-serif;}
hr{background-color:#000;border:0;color:#000;}
hr.heavy{height:2px;}
hr.light{height:1px;}
img{border:0;display:block;}
img.right{margin:0 0 0 auto;}
img.center{border:0;margin:0 auto;}
table{border-collapse: collapse;}
table th,table td{padding: 3px 7px 2px 7px;}
table th{background-color: lightgrey;}
table.center{margin-left:auto; margin-right:auto;}
.center{text-align:center;}
.right{text-align:right;}
.left{text-align:left;}
.tableborder,.tableborder td,.tableborder th{border:1px solid #000;}
.underline{text-decoration:underline;}
</style>
</head>
<body>
<header>
<hgroup>
<h1>      NumTheory</h1>
<h2>      &copy;  2009,2012-2013,2016-2018,2021,2022  John Abbott,  Anna M. Bigatti</h2>
<h3>      GNU Free Documentation License, Version 1.2</h3>
</hgroup>
</header>
<article>

<nav>
<div class="body" id="body">

    <ul>
    <li><a href="#toc1">User documentation</a>
      <ul>
      <li><a href="#toc2">Generalities</a>
      </li>
      <li><a href="#examples">Examples</a>
      </li>
      <li><a href="#toc4">The Functions Available For Use</a>
        <ul>
        <li><a href="#toc5">GCD, LCM, etc.</a>
        </li>
        <li><a href="#toc6">Prime generation and tests</a>
        </li>
        <li><a href="#toc7">Factorization</a>
        </li>
        <li><a href="#toc8">Other Functions on Integers</a>
        </li>
        <li><a href="#toc9">Functions on Rationals</a>
        </li>
        <li><a href="#toc10">Continued Fractions</a>
        </li>
        <li><a href="#toc11">Chinese Remaindering -- Integer Reconstruction</a>
        </li>
        <li><a href="#toc12">Rational Reconstruction</a>
        </li>
        </ul>
      </li>
      </ul>
    </li>
    <li><a href="#toc13">Maintainer Documentation</a>
    </li>
    <li><a href="#toc14">Bugs, Shortcomings, etc.</a>
    </li>
    <li><a href="#toc15">Main changes</a>
    </li>
    </ul>

</div>
</nav>
<div class="body" id="body">
<p>
      <center><a href="index.html">CoCoALib Documentation Index</a></center>
</p>

<section~A~>
<h1></h1>
<section id="toc1">
<h2>User documentation</h2>

<section id="toc2">
<h3>Generalities</h3>

<p>
The functions in the <code>NumTheory</code> file are predominantly basic
operations from number theory.  Most of the functions may be
applied to machine integers or big integers (<em>i.e.</em> values of
type <a href="BigInt.html"><code>BigInt</code></a>).  Please recall that computational number theory is
not the primary remit of CoCoALib, so do not expect to find a
complete collection of operations here -- you would do better to
look at Victor Shoup's NTL (Number Theory Library), or PARI/GP,
or some other specialized library/system.
</p>
<p>
See also <a href="BigIntOps.html"><code>BigIntOps</code></a> for very basic arithmetic operations on integers,
and <a href="BigRat.html"><code>BigRat</code></a> for very basic arithmetic operations on rational numbers.
</p>

</section>
<section id="examples">
<h3>Examples</h3>

<ul>
<li><a href="../../examples/index.html#ex-NumTheory1.C">ex-NumTheory1.C</a>
</li>
</ul>

</section>
<section id="toc4">
<h3>The Functions Available For Use</h3>

<p>
Several of these functions give errors if they are handed unsuitable values:
unless otherwise indicated below the error is of type <code>ERR::BadArg</code>.
All functions expecting a modulus will throw an error if the modulus is
less than 2 (or an <code>unsigned long</code> value too large to fit into a <code>long</code>).
</p>

<section id="toc5">
<h4>GCD, LCM, etc.</h4>

<p>
The main functions available are:
</p>

<ul>
<li><code>gcd(m,n)</code> computes the non-negative gcd of <code>m</code> and <code>n</code>.  If both args are machine integers, the result is of type <code>long</code> (or error if it does not fit); otherwise result is of type <a href="BigInt.html"><code>BigInt</code></a>.
</li>
<li><code>IsCoprime(m,n)</code> returns <code>true</code> iff <code>abs(gcd(m,n)) == 1</code>
</li>
<li><code>ExtGcd(a,b,m,n)</code> computes the non-negative gcd of <code>m</code> and <code>n</code>; also sets <code>a</code> and <code>b</code> so that <code>gcd = a*m+b*n</code>.  If <code>m</code> and <code>n</code> are machine integers then <code>a</code> and <code>b</code> must be of type (signed) <code>long</code>.  If <code>m</code> and <code>n</code> are of type <a href="BigInt.html"><code>BigInt</code></a> then <code>a</code> and <code>b</code> must also be of type <a href="BigInt.html"><code>BigInt</code></a>.  The cofactors <code>a</code> and <code>b</code> satisfy <code>abs(a) &lt;= abs(n)/2g</code> and <code>abs(b) &lt;= abs(m)/2g</code> where <code>g</code> is the gcd (inequalities are strict if possible).  Error if <code>m=n=0</code>.
</li>
<li><code>InvMod(r,m)</code> computes the least positive inverse of <code>r</code> modulo <code>m</code>; throws error (<code>ERR::DivByZero</code>) if the inverse does not exist.  Throws error (<code>ERR::BadModulus</code>) if <code>m &lt; 2</code> (or too big for <code>long</code>).  Result is of type <code>long</code> if <code>m</code> is a machine integer; otherwise result is of type <a href="BigInt.html"><code>BigInt</code></a>.
</li>
<li><code>InvMod(r,m, RtnZeroOnError)</code> same as <code>InvMod(r,m)</code> except that it returns 0 if the inverse does not exist; <code>RtnZeroOnError</code> comes from an enum.
</li>
<li><code>InvModNoArgCheck(r,m)</code> computes the least positive inverse of <code>r</code> modulo <code>m</code>; <strong>ASSUMES</strong> <code>0 &lt;= r &lt; m</code> and <code>2 &lt;= m &lt;= MaxLong</code>; result is a <code>long</code>.  Throws error <code>ERR::DivByZero</code> if <code>gcd(r,m)</code> is not 1.
</li>
<li><code>lcm(m,n)</code> computes the non-negative lcm of <code>m</code> and <code>n</code>.  If both args are machine integers, the result is of type <code>long</code>; otherwise result is of type <a href="BigInt.html"><code>BigInt</code></a>.  Gives error <code>ERR::ArgTooBig</code> if the lcm of two machine integers is too large to fit into a <code>long</code>.
</li>
</ul>

<p>
There is a class called <code>CoprimeFactorBasis_BigInt</code> for computing a coprime
factor basis of a set of integers:
</p>

<ul>
<li><code>CoprimeFactorBasis_BigInt()</code>  default ctor; base is initially empty.
</li>
<li><code>CFB.myAddInfo(n)</code>  use also the integer <code>n</code> when computing the factor base.
</li>
<li><code>CFB.myAddInfo(v)</code>  use also the elements of <code>std::vector&lt;long&gt; v</code> or <code>std::vector&lt;BigInt&gt; v</code> when computing the factor base.
</li>
<li><code>FactorBase(CFB)</code>  returns the factor base obtained so far (as <code>vector&lt;BigInt&gt;</code>).
</li>
</ul>

</section>
<section id="toc6">
<h4>Prime generation and tests</h4>

<p>
These functions are in <code>NumTheory-prime</code>.  The functions
<code>NextPrime</code>, <code>PrevPrime</code> and <code>RandomSmallPrime</code> each produce a
result of type <code>SmallPrime</code> (essentially a <code>long</code> which is known
to be prime).
</p>

<ul>
<li><code>eratosthenes(n)</code> build <code>vector&lt;bool&gt;</code> sieve of Eratosthenes up to <code>n</code>; entry <code>k</code> corresponds to integer <code>2*k+1</code>; max valid index is <code>n/2</code>
</li>
<li><code>EratosthenesRange(lo, hi)</code> build <code>vector&lt;bool&gt;</code> sieve of Eratosthenes from <code>lo</code> up to <code>hi</code>; if <code>lo</code> is odd, it is replaced by <code>lo+1</code>; similarly for <code>hi</code>.  In returned vector entry <code>k</code> corresponds to integer <code>lo+2*k</code>; max valid index is <code>(hi-lo)/2</code>
</li>
<li><code>IsPrime(n)</code> tests the positive number <code>n</code> for primality (may be <strong>very slow</strong> for larger numbers).  Gives error if <code>n &lt;= 0</code>.
</li>
<li><code>IsProbPrime(n)</code> tests the positive number <code>n</code> for primality (fairly fast for large numbers, but in very rare cases may falsely declare a number to be prime).  Gives error if <code>n &lt;= 0</code>.
</li>
<li><code>IsProbPrime(n,iters)</code> tests the positive number <code>n</code> for primality; performs <code>iters</code> iterations of the Miller-Rabin test (default value is 25).  Gives error if <code>n &lt;= 0</code>.
</li>
<li><code>NextPrime(n)</code> and <code>PrevPrime(n)</code> compute next or previous positive prime (fitting into a machine <code>long</code>).  <code>NextPrime</code> returns 0 if no next "small" prime exists; <code>PrevPrime</code> throws <code>OutOfRange</code> if arg is less than 3.  Both throw <code>BadArg</code> if <code>n &lt; 0</code>.
</li>
<li><code>RandomSmallPrime(N)</code> -- generate a (uniform) random prime <strong>from 5 up to <code>N</code>;</strong> error if <code>N &lt; 5</code> or <code>N &gt;= 2^31</code>.  Result is of type <code>SmallPrime</code> (essentially a <code>long</code>).
</li>
<li><code>NextProbPrime(N)</code> and <code>PrevProbPrime(N)</code> compute next or previous positive probable prime (uses <code>IsProbPrime</code>).  <code>PrevProbPrime</code> throws <code>OutOfRange</code> error if <code>0 &lt;= N &lt; 3</code>.  Both throw <code>BadArg</code> error if <code>N &lt; 0</code>.
</li>
<li><code>NextProbPrime(N,iters)</code> and <code>PrevProbPrime(N,iters)</code> compute next or previous positive probable prime (uses <code>IsProbPrime</code> with second arg <code>iters</code>).  <code>PrevProbPrime</code> throws <code>OutOfRange</code> error if <code>0 &lt;= N &lt; 3</code>.  Both throw <code>BadArg</code> error if <code>N &lt; 0</code>.
</li>
</ul>

<p>
There are also <strong>iterators for generating primes</strong> (or almost primes) in increasing order:
</p>

<ul>
<li><code>PrimeSeq()</code>  the sequence of primes starting with 2.
</li>
<li><code>PrimeSeqForCRT()</code>  a sequence of primes starting with some "large" value, and going upwards.
</li>
<li><code>FastFinitePrimeSeq()</code>  a sequence containing all primes up to some limit (much faster than <code>PrimeSeq</code>); limit is given by mem fn <code>myLastPrime</code>.
</li>
<li><code>FastMostlyPrimeSeq()</code>  a sequence containing all primes and a few composites (much faster than <code>PrimeSeq</code>).
</li>
<li><code>NoSmallFactorSeq()</code>  a sequence of positive integers which have no small factors.
</li>
</ul>

<p>
If <code>pseq</code> is one of these iterator objects then
</p>

<ul>
<li><code>*pseq</code> gives the current prime in the sequence (as a value of type <code>SmallPrime</code> or <code>long</code> for <code>FastMostlyPrimeSeq</code> and <code>NoSmallFactorSeq</code>)
</li>
<li><code>++pseq</code> advances 1 step along the sequence
</li>
<li><code>IsEnded(pseq)</code>  returns <code>true</code> if the end of the sequence has been reached
</li>
<li><code>CurrPrime(pseq)</code> same as <code>*pseq</code> (only for <code>PrimeSeq</code> and <code>PrimeSeqForCRT</code>)
</li>
<li><code>NextPrime(pseq)</code>  advances 1 step along the sequence, and returns the new "current prime" (only for <code>PrimeSeq</code> and <code>PrimeSeqForCRT</code>)
</li>
</ul>

</section>
<section id="toc7">
<h4>Factorization</h4>

<ul>
<li><code>factor(n)</code> finds the complete factorization of <code>n</code> (<strong>WARNING</strong> may be <strong>very slow</strong> for large numbers); NB <strong>implementation incomplete</strong>
</li>
<li><code>factor_TrialDiv(n,limit)</code> finds small prime factors of <code>n</code> (up to &amp; including the specified <code>limit</code>); result is a <code>factorization</code> object.  Gives error if <code>limit</code> is not positive or too large to fit into a <code>long</code>.  See also <code>MultiplicityOf2</code> in <a href="BigIntOps.html"><code>BigIntOps</code></a>.
</li>
<li><code>factor_PollardRho(n,niters)</code> attempt to find a (single) factor of <code>n</code> (not nec. prime) using at most <code>niters</code> iterations; returns "empty" factorization if no factor was found; factor found may not be prime.
</li>
<li><code>SumOfFactors(n,k)</code> compute sum of <code>k</code>-th powers of positive factors of <code>n</code>
</li>
<li><code>SmallestNonDivisor(n)</code>  finds smallest (positive prime) non-divisor of <code>n</code>; if <code>n=0</code> throws <code>ERR::NotNonZero</code>.
</li>
<li><code>IsSqFree(n)</code> returns <code>true</code> if <code>n</code> is square-free, otherwise <code>false</code>; for <code>BigInt</code> result is a <a href="bool3.html"><code>bool3</code></a>
</li>
<li><code>FactorMultiplicity(b,n)</code> find largest <code>k</code> such that <code>power(b,k)</code> divides <code>n</code> (error if <code>b &lt; 2</code> or <code>n</code> is zero)
<p></p>
=====Pollard Rho Sequence=====
<p></p>
</li>
<li><code>PollardRhoSeq(N, start, incr)</code>  create a sequence starting from <code>start</code> and with increment <code>incr</code>
</li>
<li><code>PRS.myAdvance(k)</code> advance the sequence by <code>k</code> steps
</li>
<li><code>GetFactor(PRS)</code> returns a factor of <code>N</code> (may be 1 or <code>N</code>)
</li>
<li><code>GetNumIters(PRS)</code> returns number of steps/iters performed
</li>
</ul>

</section>
<section id="toc8">
<h4>Other Functions on Integers</h4>

<ul>
<li><code>EulerPhi(n)</code> computes Euler's <em>totient</em> function of the positive number <code>n</code> (<em>i.e.</em> the number of integers up to <code>n</code> which are coprime to <code>n</code>, or the degree of the <code>n</code>-th cyclotomic polynomial).  Gives error if <code>n &lt;= 0</code>.
</li>
<li><code>PrimitiveRoot(p)</code> computes the least positive primitive root for the positive prime <code>p</code>.  Gives error if <code>p</code> is not a positive prime.  <strong>WARNING</strong> May be <strong>very slow</strong> for large <code>p</code> (because it must factorize <code>p-1</code>).
</li>
<li><code>KroneckerSymbol(res,mod)</code>  (test if <code>res</code> is a quadratic residue) computes the Kronecker symbol, generalization of Jacobi symbol, generalization of Legendre symbol
</li>
<li><code>MultiplicativeOrderMod(res,mod)</code> computes multiplicative order of <code>res</code> modulo <code>mod</code>.  Throws error <code>ERR::BadArg</code> if <code>mod &lt; 2</code> or <code>gcd(res,mod)</code> is not 1.
</li>
<li><code>PowerMod(base,exp,modulus)</code>  computes <code>base</code> to the power <code>exp</code> modulo <code>modulus</code>; result is least non-negative residue.  If <code>modulus</code> is a machine integer then the result is of type <code>long</code> (or error if it does not fit), otherwise the result is of type <a href="BigInt.html"><code>BigInt</code></a>.  Gives error if <code>modulus &lt;= 1</code>.  Gives <code>ERR::DivByZero</code> if <code>exp</code> is negative and <code>base</code> cannot be inverted.  If <code>base</code> and <code>exp</code> are both zero, it produces 1.
</li>
<li><code>BinomialRepr(N,r)</code> produces the repr of <code>N</code> as a sum of binomial coeffs with "denoms" <code>r, r-1, r-2, ...</code>
</li>
</ul>

</section>
<section id="toc9">
<h4>Functions on Rationals</h4>

<ul>
<li><code>SimplestBigRatBetween(A,B)</code> computes the simplest rational between <code>A</code> and <code>B</code>
</li>
<li><code>SimplestBinaryRatBetween(A,B)</code> computes the simplest binary rational between <code>A</code> and <code>B</code>; result is a rational of form N*2^k where the integer N is minimal.
</li>
</ul>

</section>
<section id="toc10">
<h4>Continued Fractions</h4>

<p>
Several of these functions give errors if they are handed unsuitable values:
unless otherwise indicated below the error is of type <code>ERR::BadArg</code>.
</p>
<p>
Recall that any real number has an expansion as a <strong>continued fraction</strong>
(<em>e.g.</em> see Hardy &amp; Wright for definition and many properties).  This expansion
is finite for any rational number.  We adopt the following conventions which
guarantee that the expansion is unique:
</p>

<ul>
<li>the last partial quotient is greater than 1 (except for the expansion of integers &lt;= 1)
</li>
<li>only the very first partial quotient may be non-positive.
</li>
</ul>

<p>
For example, with these conventions the expansion of -7/3 is (-3, 1, 2).
</p>
<p>
The main functions available are:
</p>

<ul>
<li><code>ContFracIter(q)</code> constructs a new continued fraction iterator object
</li>
<li><code>IsEnded(CFIter)</code> true iff the iterator has moved past the last <em>partial quotient</em>
</li>
<li><code>IsFinal(CFIter)</code> true iff the iterator is at the last <em>partial quotient</em>
</li>
<li><code>quot(CFIter)</code> gives the current <em>partial quotient</em> as a <a href="BigInt.html"><code>BigInt</code></a> (or throws <code>ERR::IterEnded</code>)
</li>
<li><code>*CFIter</code> gives the current <em>partial quotient</em> as a <a href="BigInt.html"><code>BigInt</code></a> (or throws <code>ERR::IterEnded</code>)
</li>
<li><code>++CFIter</code> moves to next <em>partial quotient</em> (or throws <code>ERR::IterEnded</code>)
<p></p>
</li>
<li><code>ContFracApproximant()</code> for constructing a rational from its continued fraction quotients
</li>
<li><code>CFA.myAppendQuot(q)</code> appends the quotient <code>q</code> to the continued fraction
</li>
<li><code>CFA.myRational()</code> returns the rational associated to the continued fraction
<p></p>
</li>
<li><code>CFApproximantsIter(q)</code> constructs a new continued fraction approximant iterator
</li>
<li><code>IsEnded(CFAIter)</code> true iff the iterator has moved past the last "partial quotient"
</li>
<li><code>*CFAIter</code> gives the current continued fraction approximant as a <a href="BigRat.html"><code>BigRat</code></a> (or throws <code>ERR::IterEnded</code>)
</li>
<li><code>++CFAIter</code> moves to next approximant (or throws <code>ERR::IterEnded</code>)
<p></p>
</li>
<li><code>CFApprox(q,eps)</code> gives the simplest cont. frac. approximant to <code>q</code> with relative error at most <code>eps</code>
</li>
</ul>

</section>
<section id="toc11">
<h4>Chinese Remaindering -- Integer Reconstruction</h4>

<p>
CoCoALib offers the class <code>CRTMill</code> for reconstructing an integer from
several residue-modulus pairs via Chinese Remaindering.  At the moment the
moduli from distinct pairs must be coprime.
</p>
<p>
The operations available are:
</p>

<ul>
<li><code>CRTMill()</code> ctor; initially the residue is 0 and the modulus is 1
</li>
<li><code>CRT.myAddInfo(res,mod)</code> give a new residue-modulus pair to the <code>CRTMill</code> (error if <code>mod</code> is not coprime to all previous moduli)
</li>
<li><code>CRT.myAddInfo(res,mod,CRTMill::CoprimeModulus)</code> give a new residue-modulus pair to the <code>CRTMill</code> asserting that <code>mod</code> is coprime to all previous moduli --  <code>CRTMill::CoprimeModulus</code> is a constant
</li>
<li><code>CombinedResidue(CRT)</code> the combined residue with absolute value less than or equal to <code>CombinedModulus(CRT)/2</code>
</li>
<li><code>CombinedModulus(CRT)</code> the product of the moduli of all pairs given to the mill
</li>
</ul>

</section>
<section id="toc12">
<h4>Rational Reconstruction</h4>

<p>
CoCoALib offers two heuristic methods for reconstructing rationals from
residue-modulus pairs; they have the same user interface but internally one
algorithm is based on continued fractions while the other uses lattice
reduction.  The methods are heuristic, so may (rarely) produce an incorrect
result.
</p>
<p>
<strong>NOTE</strong> the heuristic requires the (combined) modulus to be a certain
amount larger than strictly necessary to reconstruct the correct
answer (assuming perfect bounds are known).  In practice, this means
that <strong>the methods always fail if the combined modulus is too small</strong>.
</p>
<p>
The constructors available are:
</p>

<ul>
<li><code>RatReconstructByContFrac()</code> ctor for continued fraction method mill log-epsilon equal to 20
</li>
<li><code>RatReconstructByContFrac(LogEps)</code> ctor for continued fraction method mill with given log-epsilon (must be at least 3)
</li>
<li><code>RatReconstructByLattice(SafetyFactor)</code> ctor for lattice method mill with given <code>SafetyFactor</code> (0 --&gt; use default)
</li>
</ul>

<p>
The operations available are:
</p>

<ul>
<li><code>reconstructor.myAddInfo(res,mod)</code> give a new residue-modulus pair to the reconstructor
</li>
<li><code>IsConvincing(reconstructor)</code> gives <code>true</code> iff the mill can produce a <em>convincing</em> result
</li>
<li><code>ReconstructedRat(reconstructor)</code> gives the reconstructed rational (or an error if <code>IsConvincing</code> is not true).
</li>
<li><code>BadMFactor(reconstructor)</code> gives the "bad factor" of the combined modulus.
</li>
</ul>

<p>
There is also a function for deterministic rational reconstruction which
requires certain bounds to be given in input.  It uses the continued fraction
method.
</p>

<ul>
<li><code>RatReconstructWithBounds(e,P,Q,res,mod)</code> where <code>e</code> is upper bound for number of "bad" moduli, <code>P</code> and <code>Q</code> are upper bounds for numerator and denominator of the rational to be reconstructed, and <code>(res[i],mod[i])</code> is a residue-modulus pair with distinct moduli being coprime.
</li>
</ul>

</section>
</section>
</section>
<section id="toc13">
<h2>Maintainer Documentation</h2>

<ul>
<li>Correctness of <code>ExtendedEuclideanAlg</code> is not immediately clear,
because the cofactor variables could conceivably overflow -- in fact
this cannot happen (at least on a binary computer): for a proof see
Shoup's book <em>A Computational Introduction to Number Theory and Algebra</em>,
in particular Theorem 4.3 and the comment immediately following it.  There is
just one line where a harmless "overflow" could occur -- it is commented in
the code.
<p></p>
</li>
<li>I have decided to make <code>ExtGcd</code> give an error if the inputs are both zero
because this is an exceptional case, and so should be handled specially.  I
note that <code>mpz_exgcd</code> accepts this case, and returns two zero cofactors; so if we
decide to accept this case, we should do the same -- this all fits in well with
the (reasonable/good) principle that "zero inputs have zero cofactors".
<p></p>
-  Several functions are more complicated than you might expect because I wanted them
to be correct for all possible machine integer inputs (<em>e.g.</em> including the
most negative <code>long</code> value).
<p></p>
</li>
<li>In some cases the function which does all the work is implemented as a file
local function operating on <code>unsigned long</code> values: the function should
normally be used only via the "dispatch" functions whose args are of type
<a href="MachineInt.html"><code>MachineInt</code></a> or <a href="BigInt.html"><code>BigInt</code></a>.
</li>
</ul>

<ul>
<li>The fns for primes (testing and generating) are in the file <code>NumTheory-prime</code>.
<p></p>
</li>
<li>The impl of <code>eratosthenes</code> is fairly straightforward given that I chose
to represent only odd numbers in the table: the <code>k</code>-th entry corresponds
to the integer <code>2*k+1</code>.  Overflow cannot occur: recall that the table
size is at most half the biggest <code>long</code>.  I'm hoping that C++11 will
avoid the cost of copying the result upon returning.  Anyway, I think the
code is a decent compromise between readability, speed and space efficiency.
The impl of <code>EratosthenesRange</code> is similar but the table covers just
the given range (only odd numbers are represented; index 0 corresponds to
smallest odd integer greater than or equal to the start of the range).
</li>
</ul>

<ul>
<li>The "prime sequence" classes are a bit messier than I'd like.
It was delicate getting correct the switch-over from one technique to the
other (in those classes where 2 techniques were used).  The limit of 23
for <code>NoSmallFactorSeq</code> is somewhat arbitrary.  Not sure the code is
32-bit safe.
</li>
</ul>

<ul>
<li>The continued fraction functions are all pretty simple.  The only tricky
part is that the "end" of the <code>ContFracIter</code> is represented by both
<code>myFrac</code> and <code>myQuot</code> being zero.  This means that a newly created
iterator for zero is already ended.
<p></p>
</li>
<li><code>CFApproximantsIter</code> delegates most of the work to <code>ContFracIter</code>.
</li>
</ul>

</section>
<section id="toc14">
<h2>Bugs, Shortcomings, etc.</h2>

<ul>
<li>Several functions return <code>long</code> values when perhaps <code>unsigned long</code>
would possibly be better choice (since it offers a greater range, and
in the case of <code>gcd</code> it would permit the fn to return a result always,
rather than report "overflow").  The choice of return type was dictated
by the coding conventions, which were in turn dictated by the risks of nasty
surprises to unwary users unfamiliar with the foibles of unsigned values in C++.
<p></p>
</li>
<li><code>NextPrime</code> has dodgy semantics: what happens when the end of the
iterator is reached?  In fact, all the non-finite "prime seq" iterators
do not handle end-of-iterator properly!
<p></p>
</li>
<li>Should there also be procedural forms of functions which return <a href="BigInt.html"><code>BigInt</code></a> values?
(<em>e.g.</em> gcd, lcm, InvMod, PowerMod, and so on).  (2016-06-27 this will probably become irrelevant when using "move" semantics in C++11).
<p></p>
</li>
<li>Certain implementations of <code>PowerMod</code> should be improved (<em>e.g.</em> to use
<code>PowerModSmallModulus</code> whenever possible).  Is behaviour for 0^0 correct?
<p></p>
</li>
<li><code>KroneckerSymbol</code> I have chosen to make available just <code>KroneckerSymbol</code>
rather than the more widely-known <code>LegendreSymbol</code> because GMP makes
Kronecker available, and it is always defined; whereas
<code>LegendreSymbol</code> would have to check that its 2nd arg is a prime
(which would dominate the cost of the call)
<p></p>
</li>
<li><code>LucasTest</code> should produce a certificate, and be made publicly accessible.
<p></p>
</li>
<li>How should the cont frac iterators be printed out???
<p></p>
</li>
<li><code>ContFracIter</code> could be rather more efficient for rationals having
very large numerator and denominator.  One way would be to compute with
num and den divided by the same large factor (probably a power of 2),
and taking care to monitor how accurate these "scaled" num and den are.
I'll wait until there is a real need before implementing (as I expect
it will turn out a bit messy).
<p></p>
</li>
<li><code>CFApproximantsIter::operator++()</code> should be made more efficient.
</li>
</ul>

</section>
<section id="toc15">
<h2>Main changes</h2>

<p>
<strong>2022</strong>
</p>

<ul>
<li>Feb (v0.99720):
 <ul>
 <li><code>SmoothFactor</code> has been renamed <code>factor_TrialDiv</code>
 </li>
 <li>added <code>factor_PollardRho</code>
-
 </li>
 </ul>
</li>
</ul>

</section>
</section>
</div>
</article></body></html>
