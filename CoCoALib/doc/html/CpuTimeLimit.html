<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>      CpuTimeLimit</title>
<meta name="generator" content="https://txt2tags.org">
<link rel="stylesheet" href="cocoalib-doc.css">
<style type="text/css">
blockquote{margin: 1em 2em; border-left: 2px solid #999;
  font-style: oblique; padding-left: 1em;}
blockquote:first-letter{margin: .2em .1em .1em 0; font-size: 160%; font-weight: bold;}
blockquote:first-line{font-weight: bold;}
body{font-family: sans-serif;}
hr{background-color:#000;border:0;color:#000;}
hr.heavy{height:2px;}
hr.light{height:1px;}
img{border:0;display:block;}
img.right{margin:0 0 0 auto;}
img.center{border:0;margin:0 auto;}
table{border-collapse: collapse;}
table th,table td{padding: 3px 7px 2px 7px;}
table th{background-color: lightgrey;}
table.center{margin-left:auto; margin-right:auto;}
.center{text-align:center;}
.right{text-align:right;}
.left{text-align:left;}
.tableborder,.tableborder td,.tableborder th{border:1px solid #000;}
.underline{text-decoration:underline;}
</style>
</head>
<body>
<header>
<hgroup>
<h1>      CpuTimeLimit</h1>
<h2>      &copy;  2017-2021  John Abbott, Anna M. Bigatti</h2>
<h3>      GNU Free Documentation License, Version 1.2</h3>
</hgroup>
</header>
<article>

<nav>
<div class="body" id="body">

    <ul>
    <li><a href="#examples">Examples</a>
    </li>
    <li><a href="#toc2">User documentation for CpuTimeLimit</a>
      <ul>
      <li><a href="#constructors">Constructor</a>
      </li>
      <li><a href="#operations">Operations</a>
      </li>
      <li><a href="#exceptions">Exception</a>
      </li>
      </ul>
    </li>
    <li><a href="#toc6">Maintainer documentation</a>
    </li>
    <li><a href="#toc7">Bugs, shortcomings and other ideas</a>
    </li>
    <li><a href="#toc8">Main changes</a>
    </li>
    </ul>

</div>
</nav>
<div class="body" id="body">
<p>
      <center><a href="index.html">CoCoALib Documentation Index</a></center>
</p>

<section~A~>
<h1></h1>
<section id="examples">
<h2>Examples</h2>

<ul>
<li><a href="../../examples/index.html#ex-CpuTimeLimit1.C">ex-CpuTimeLimit1.C</a>
</li>
<li><a href="../../examples/index.html#ex-CpuTimeLimit2.C">ex-CpuTimeLimit2.C</a>
</li>
</ul>

</section>
<section id="toc2">
<h2>User documentation for CpuTimeLimit</h2>

<p>
An object of type <code>CpuTimeLimit</code> may be used to "limit" the CPU time
taken by a computation: if the computation takes too long then an
exception (of type <code>CoCoA::TimeoutException</code>) is thrown.
</p>
<p>
When creating a <code>CpuTimeLimit</code> object you must specify a time limit
in seconds as a positive <code>double</code>: <em>e.g.</em> <code>CpuTimeLimit CheckTime(10)</code>.
</p>
<p>
You must tell the <code>CheckTime</code> object explicitly when it should check whether
the time limit has been reached by calling <code>CheckTime()</code>, <em>i.e.</em> by calling
its member function <code>operator()</code>.  If the time limit has been reached,
it throws an exception of type <code>CoCoA::TimeoutException</code> (derived from
<code>CoCoA::ErrorInfo</code>); otherwise the call does nothing (other than the check).
</p>
<p>
An optional second parameter to the ctor specifies the "variability" of
time between successive iterations: <em>e.g.</em> if the checks are in a loop
where each iteration takes more or less the same time then the variability
is low (<code>IterationVariability::low</code>); if the iterations can vary greatly in
computation time then the variability is high  (<code>IterationVariability::high</code>);
by default the variability is medium (<code>IterationVariability::medium</code>).
</p>
<p>
The typical use is with a potentially long loop.  Just before the loop
one creates the <code>CpuTimeLimit</code> object, then at the start of each
iteration inside the loop one calls <code>operator()</code>.
</p>
<p>
<strong>IMPORTANT</strong> CoCoALib checks for timeout <strong>only when</strong> the member
function <code>CpuTimeLimit::operator()</code> is called; so CoCoALib
<strong>will not notice that time-out has occurred between successive calls</strong>
to <code>operator()</code>.
</p>
<p>
It is possible to use a single <code>CpuTimeLimit</code> object for several
loops, but then it is best to call <code>myPrepareForNewLoop</code> just before
entering each new loop; the variability of the iterations of that loop
can be specified.
</p>

<section id="constructors">
<h3>Constructor</h3>

<p>
There is one real constructor, and one pseudo-constructor:
</p>

<ul>
<li><code>CpuTimeLimit(seconds)</code> where <code>seconds</code> is a positive <code>double</code>; the measurement of CPU use begins immediately; there is an upper limit of one million seconds.
</li>
<li><code>CpuTimeLimit(seconds, variability)</code> as above, but specify how variable time between successive checks might be
</li>
<li><code>NoCpuTimeLimit()</code> returns a <code>CpuTimeLimit</code> object which has infinite timeout
</li>
</ul>

<p>
Variability should be: <code>IterationVariability::low</code> if successive iterations take more or less the same time;
<code>IterationVariability::high</code> if successive iterations can take widely differing amounts of time.  The default
is <code>IterationVariability::medium</code> which indicates some sort of compromise.
</p>

</section>
<section id="operations">
<h3>Operations</h3>

<p>
Let <code>CheckForTimeout</code> be an object of type <code>CpuTimeLimit</code>.
There are two operations:
</p>

<ul>
<li><code>CheckForTimeout(context)</code> -- does nothing unless timeout has occurred, when it throws a <code>TimeoutException</code> object; <code>context</code> is a string literal which is copied into the "context" field of the exception
</li>
<li><code>CheckForTimeout.myPrepareForNewLoop()</code> -- if the same <code>CpuTimeLimit</code> object is to be used inside more than one loop, then call this before every loop (except the first one)
</li>
<li><code>CheckForTimeout.myPrepareForNewLoop(v)</code> -- like <code>myPrepareForNewLoop</code> but also specify a "variability" for the new loop
</li>
<li><code>IsUnlimited(CheckForTimeout)</code>  -- return <code>true</code> iff <code>CheckForTimeout</code> was created by <code>NoCpuTimeLimit</code>
</li>
</ul>

</section>
<section id="exceptions">
<h3>Exception</h3>

<p>
There is one class for exceptions:
</p>

<ul>
<li><code>TimeoutException(context)</code> -- the arg <code>context</code> is a string literal indicating where the time-out was detected (usually it is a good idea to use the name of the function which was interrupted)
</li>
</ul>

<p>
The class <code>TimeoutException</code> derives from <code>ErrorInfo</code>.
</p>

</section>
</section>
<section id="toc6">
<h2>Maintainer documentation</h2>

<p>
This is the fourth design.  The first was based on SIGVTALRM, but it was not
clear how portable that would be.  The second was based on <code>CheckForInterrupt</code>,
but the calls to <code>CpuTime</code> were too costly (and it depended on a global
variable).  The third design was based on <code>ProgressReporter</code>: it assumed
that the times between successive clock checks do not vary too much.
This new fourth design revises the third, and lets the caller specify
the "variability" of time between successive checks.
</p>
<p>
The idea is to check the actual cpu time only occasionally, and not every
time <code>operator()</code> is called.  It uses a similar strategy to that of
<code>ProgressReporter</code>; based on the variability, an estimate of how many iters
can safely be performed before the next check is used to 
which assumes that calls to <code>operator()</code> occur at
fairly regular intervals.
</p>
<p>
The private data field <code>myInterval</code> has a special role if its value is
negative: it means that the <code>CpuTimeLimit</code> object has infinite time-out,
so should never check cpu usage.
</p>

</section>
<section id="toc7">
<h2>Bugs, shortcomings and other ideas</h2>

<p>
Perhaps offer a version which uses only elapsed time?  This should be easy to implement!
</p>
<p>
Inconvenient having to pass <code>CpuTimeLimit</code> as explicit parameters;
but how else to do this in a threadsafe way?
</p>
<p>
A normal call to <code>CpuTime()</code> may not work as desired in a multithreaded
context.  It is not clear how to solve this portably.
</p>

</section>
<section id="toc8">
<h2>Main changes</h2>

<p>
<strong>2019</strong>
</p>

<ul>
<li>Dec (v.0.99650): big revision, introduced "variability" factor.
<p></p>
<strong>2018</strong>
</li>
<li>May (v0.99570): major revision, now cheaper and multithread compatible.
<p></p>
<strong>2017</strong>
</li>
<li>July (v0.99560): first release; major revision
</li>
</ul>

</section>
</section>
</div>
</article></body></html>
