#!/bin/bash

git submodule update --init --recursive boost

#compile boost
cd boost
./bootstrap.sh --prefix=$(realpath ../.boost_lib) --with-libraries="filesystem,system,thread"
./b2 -j4 architecture=x86 address-model=32_64 variant=release threading=multi link=static install
cd ..
