#!/bin/bash

git submodule update --init gsl

#compile gsl
cd gsl
./autogen.sh
./configure --prefix=$(realpath ../.gsl_lib)   
make -j4
make install
cd ..

