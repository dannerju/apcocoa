#copy ApCoCoA-server, compile cocoa5 (with ext libs), and apcocoa-packages into one folder

git submodule update --init apcocoa-packages

#create output folder '.apcocoa' and assemble the contents
mkdir .apcocoa
./apcocoa-packages/install.sh ./.cocoa5/cocoa5
cp -rL .cocoa5/* .apcocoa
cp .apcocoa_server/ApCoCoAServer .apcocoa
