//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2008 Stefan Kaspar
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#ifndef ApCoCoA_BasisTransformation_H
#define ApCoCoA_BasisTransformation_H

#include "CoCoA/ring.H"

namespace ApCoCoA
{
  namespace AlgebraicAlgorithms
  {
    /*-----------------------------------------------------------------*/
    /** \include BasisTransformation.txt                               */
    /*-----------------------------------------------------------------*/

    /*! \brief Transform a border basis into a Groebner basis.
     *
     *  This function takes a \f$\mathcal{O}_\sigma(I)\f$-border basis
     *  and returns the reduced \f$\sigma\f$-Groebner basis of \f$I\f$.
     *
     *  \param GB Will hold the computed Groebner basis polynomials.
     *  \param BB The border basis polynomials.
     */
    void TransformBBIntoGB(std::vector<CoCoA::RingElem>& GB,
                           const std::vector<CoCoA::RingElem>& BB);

    /*! \brief Transform a Groebner basis into a border basis.
     *
     *  This function takes a \f$\sigma\f$-Groebner basis of a
     *  zero-dimensional ideal \f$I\f$ as input and returns the
     *  \f$\mathcal{O}_\sigma(I)\f$-border basis.
     *
     *  \param BB Will hold the computed border basis polynomials.
     *  \param GB The Groebner basis polynomials.
     */
    void TransformGBIntoBB(std::vector<CoCoA::RingElem>& BB,
                           const std::vector<CoCoA::RingElem>& GB);
  } // end of namespace AlgebraicAlgorithms
} // end of namespace ApCoCoA

#endif
