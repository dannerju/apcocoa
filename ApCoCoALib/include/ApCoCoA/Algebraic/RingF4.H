//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2007 Daniel Heldt
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#ifndef ApCoCoA_RingF4_H
#define ApCoCoA_RingF4_H

#include "CoCoA/ring.H"
#include "CoCoA/error.H"


namespace ApCoCoA
{

namespace AlgebraicCore
{

    class RingF4Base: public CoCoA::RingBase
    {
    public: // member functions specific to RingFloat implementations
    };

   /**
     * \brief RingF4 is  a simple ring, isomorphic to Z/(2)[x]/(x^2 + x + 1 )
     *
     * This ring represents one representation of the field with 4 elements.
     * It is constructed as a field extension of F_2 modulo the polynomial x^2 +x + 1.
     * Internally this fields elements are stored as unsigned ints. These ints contain the polynomials
     * exponent vectors, e.g. 0 -> 0, 1 -> 1, 2 -> x, 3-> x+1, ... 
     *
     * @see NewRingF4
     * @see IsRingF4
     * @see CoCoA::ring
     */
     
    class RingF4: public CoCoA::ring
    {
    public:
      explicit RingF4(const RingF4Base* RingPtr);
      // Default copy ctor works fine.
      // Default dtor works fine.
    private:
      RingF4& operator=(const RingF4& rhs); ///< NEVER DEFINED -- disable assignment
    public:
      const RingF4Base* operator->() const; 
    };

   /**
     * \brief NewRingF4() creates a new RingF4 and returns it.
     *
     * @return the new created RingF4
     * @see RingF4
     * @see IsRingF4
     * @see CoCoA::ring
     */
    RingF4 NewRingF4();

   /**
     * \brief IsRingF4() checks if a ring is an instance of RingF4.
     *
     * @param RR - the ring to check
     * @return true if RR is a RingF4, false otherwise.
     * @see NewRingF4
     * @see RingF4
     * @see CoCoA::ring
     */
    bool IsRingF4(const CoCoA::ring& RR);

    inline const RingF4Base* RingF4::operator->() const
    {
      return static_cast<const RingF4Base*>(ring::operator->());
    }

} // end of namespace AlgebraicCore
} // end of namespace ApCoCoA

#endif
