//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2007 Daniel Heldt
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#ifndef ApCoCoA_RingF256_H
#define ApCoCoA_RingF256_H

#include "CoCoA/ring.H"
#include "CoCoA/error.H"


namespace ApCoCoA
{

namespace AlgebraicCore
{

    class RingF256Base: public CoCoA::RingBase
    {
    public: // member functions specific to RingFloat implementations
    };

   /**
     * \brief RingF256 is  a simple ring, isomorphic to Z/(2)[x]/(x^8 + x^7 + x^6 + x^5 + x^4 + x^3 + 1 )
     *
     * This ring represents one representation of the field with 256 elements.
     * It is constructed as a field extension of F_2 modulo the polynomial x^8 + x^7 + x^6 + x^5 + x^4 + x^3 + 1.
     * Internally this fields elements are stored as unsigned ints. These ints contain the polynomials
     * exponent vectors, e.g. 0 -> 0, 1 -> 1, 2 -> x, 3-> x+1, ... 
     *
     * @see NewRingF256
     * @see IsRingF256
     * @see CoCoA::ring
     */
     
    class RingF256: public CoCoA::ring
    {
    public:
      explicit RingF256(const RingF256Base* RingPtr);
      // Default copy ctor works fine.
      // Default dtor works fine.
    private:
      RingF256& operator=(const RingF256& rhs); ///< NEVER DEFINED -- disable assignment
    public:
      const RingF256Base* operator->() const; 
    };

   /**
     * \brief NewRingF256() creates a new RingF256 and returns it.
     *
     * @return the new created RingF256
     * @see RingF256
     * @see IsRingF256
     * @see CoCoA::ring
     */
    RingF256 NewRingF256();

   /**
     * \brief IsRingF256() checks if a ring is an instance of RingF256.
     *
     * @param RR - the ring to check
     * @return true if RR is a RingF256, false otherwise.
     * @see NewRingF256
     * @see RingF256
     * @see CoCoA::ring
     */
    bool IsRingF256(const CoCoA::ring& RR);

    inline const RingF256Base* RingF256::operator->() const
    {
      return static_cast<const RingF256Base*>(ring::operator->());
    }

} // end of namespace AlgebraicCore
} // end of namespace ApCoCoA

#endif
