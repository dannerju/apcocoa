//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2007 Daniel Heldt
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#ifndef ApCoCoA_RingF512_H
#define ApCoCoA_RingF512_H

#include "CoCoA/ring.H"
#include "CoCoA/error.H"


namespace ApCoCoA
{

namespace AlgebraicCore
{

    class RingF512Base: public CoCoA::RingBase
    {
    public: // member functions specific to RingFloat implementations
    };

   /**
     * \brief RingF512 is  a simple ring, isomorphic to Z/(2)[x]/(x^9 + x +1 )
     *
     * This ring represents one representation of the field with 512 elements.
     * It is constructed as a field extension of F_2 modulo the polynomial x^9 + x +1 .
     * Internally this fields elements are stored as unsigned ints. These ints contain the polynomials
     * exponent vectors, e.g. 0 -> 0, 1 -> 1, 2 -> x, 3-> x+1, ... 
     *
     * @see NewRingF512
     * @see IsRingF512
     * @see CoCoA::ring
     */
     
    class RingF512: public CoCoA::ring
    {
    public:
      explicit RingF512(const RingF512Base* RingPtr);
      // Default copy ctor works fine.
      // Default dtor works fine.
    private:
      RingF512& operator=(const RingF512& rhs); ///< NEVER DEFINED -- disable assignment
    public:
      const RingF512Base* operator->() const; 
    };

   /**
     * \brief NewRingF512() creates a new RingF512 and returns it.
     *
     * @return the new created RingF512
     * @see RingF512
     * @see IsRingF512
     * @see CoCoA::ring
     */
    RingF512 NewRingF512();

   /**
     * \brief IsRingF512() checks if a ring is an instance of RingF512.
     *
     * @param RR - the ring to check
     * @return true if RR is a RingF512, false otherwise.
     * @see NewRingF512
     * @see RingF512
     * @see CoCoA::ring
     */
    bool IsRingF512(const CoCoA::ring& RR);

    inline const RingF512Base* RingF512::operator->() const
    {
      return static_cast<const RingF512Base*>(ring::operator->());
    }

} // end of namespace AlgebraicCore
} // end of namespace ApCoCoA

#endif
