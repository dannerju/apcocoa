//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2008-2010,2013 X. XIU
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#ifndef ApCoCoA_Ufnarovski_H
#define ApCoCoA_Ufnarovski_H

#include "ApCoCoA/Noncommutative/NCPolynomialRing.H"
#include <iostream>
#include <set>
#include <string>
#include <vector>
using std::cout;
using std::endl;
using std::set;
using std::size_t;
using std::string;
using std::vector;


namespace ApCoCoA {
    namespace NoncommutativeAlgorithms {        
        
        
        ////////////////////////////////////////////////////////////
        //	Functions for the Ufnarovski techniques.
        ////////////////////////////////////////////////////////////
        
        //! ======== Ufnarovski graph ========
        /*! Ufnarovski graph (V,E) of M, where M is a set of words.
         *  V: the set of vertices. V consists of words which have length
         *     -1+max{len(w)|w in M} and which are reduced with respect to M.
         *  E: the set of edges. There is a (directed) edge from w1 to w2 if
         *     and only if there exist letters x,y in X such that w1*x=y*w2
         *     and w1*x is reduced with respect to M.
         *
         *  Requirement:
         *  (1) M is interreduced;
         *  (2) M is non-trivial, i.e. M is not empty and does not contain 1.
         *  (3) Since <X,y>/<M,y> is isomorphic to <X>/<M>, we assume that M
         *  does not contain any single letter (i.e. word of length 1).
         */
        
    	//========== for the ApCoCoA package gbmr ==========
        
        /*! \brief IsReduced;
         *  The function checks whether or not a word is reduced (w.r.t. M).
         *  A word w is reduced with respect to M if w does not contain any
         *  word in M.
         */
        bool IsReduced(const string & w, const set<string> & M);
        
        /*! \brief UfnVertices;
         *  The function constructs vertices V of the Ufnarovski graph of M,
         *  where M is a set of (interreduced) words generated by X.
         *	Let k=-1+max{len(w)|w in M}. Then V consists of words that have
         *	length k and that are reduced with respect to M.
         *	If M is empty or a subset of {1,a,b,...}, then V is empty.
         */
        void UfnVertices(const string & X, const set<string> & M,
                         vector<string> & V);
        
        /*! \brief UfnGraph;
         *  The function constructs the Ufnarovski graph of M.
         *  The graph is represented via adjacency list.
         *  Note that there is a (directed) edge from w1 to w2 if and only if
         *  there exist letters x,y in X such that w1*x=y*w2 and w1*x is reduced
         *  with respect to M.
         */
        void UfnGraph(const set<string> & M, const vector<string> & V,
                      vector<vector<size_t> > & adLists);
        
        /*! \brief adMatrix;
         *  The function generates an adjacency matrix of the Ufnarovski graph
         *  of M from its adjacency lists.
         *  Note that an adjacency matrix is a nxn binary matrix, where the
         *  (i,j)-th element is 1 if and only if there is an edge from wi to wj.
         */
        void adMatrix(const string & X, const set<string> & M,
                      vector<string> & V, vector<vector<size_t> > & adMat);
        
        
        //========== for the ApCoCoA package ncpoly ==========
        
        bool IsReduced(const Word & w, const set<Word> & M);
        
        void UfnVertices(const vector<size_t> & inds, const set<Word> & M,
                         vector<Word> & V);
        
        void UfnGraph(const set<Word> & M, const vector<Word> & V,
                      vector<vector<size_t> > & adLists);
        
        void adMatrix(const vector<size_t> & inds, const set<Word> & M,
                      vector<Word> & V, vector<vector<size_t> > & adMat);
        
        
        
        //========== for the ApCoCoA packages gbmr and ncpoly ==========
        
        /*! \brief findCycles;
         * 	The function searches for cycles in the graph represented
         * 	as an adjacency list.
         */
        bool findCycles(const vector<vector<size_t> > & adLists,
        				vector<vector<size_t> > & cycles);
        
        /*! \brief checkIntersection;
         * 	The function checks whether there exist two intersecting cycles.
         */
        bool checkIntersection(const vector<vector<size_t> > & cycles);
    
        /*! \brief areConnected;
         * 	The function checks whether there exists a path from cycle C1 to
         * 	cycle C2.
         * 	Assume that C1 and C1 do not intersect with each other.
         */
        bool areConnected(const vector<vector<size_t> > & adLists,
                          const vector<size_t> & C1, const vector<size_t> & C2,
                          set<vector<size_t> > & paths);
    
        
        /*! \brief function Ufnarovskij;
         *  Using Ufnarovskij graph, the function checks whether or not the 
         *  K-vector space K<X>/M is finite.
         *  It returns true if and only if the K-vector space K<X>/M is finite. 
         */
        bool Ufnarovskij(const string & X, const set<string> & M);
        
        bool Ufnarovskij(const vector<size_t> & inds, const set<Word> & M);
      

  
    } // end of namespace NoncommutativeAlgorithms
} // end of namespace ApCoCoA



#endif /* ApCoCoA_Ufnarovski_H */
