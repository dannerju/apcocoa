//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2006, 2007, 2008 Daniel Heldt, 2008-2009 Jan Limbeck
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 US

#ifndef ApCoCoA_ABM_H
#define ApCoCoA_ABM_H

#include "ApCoCoA/Numerical/Internal/DoubleDenseMatrix.H"
#include "ApCoCoA/Numerical/Internal/EvaluatedPolynomialsABM.H"
#include "ApCoCoA/Numerical/OptionsABMAVI.H"
#include "CoCoA/SparsePolyRing.H"
#include "CoCoA/CoCoA4io.H" // CoCoA::PolyList

namespace ApCoCoA {

namespace NumericalAlgorithms{

  
  
  /**
  * \brief ComputeIdealOfPointsABM computes approximate vanishing ideals
   *
   *  ComputeIdealOfPointsABM - this method computes a basis for a approximate vanishing
   *  ideal.
   *  There are different versions of this algorithm implemented and various
   *  options, explained below.  
   *
   * @param points - The set of points to compute the apVI on. Each row represents one point, 
   * so the number of columns defines the points' dimension.
   * @param eps - the maximum error in a basis polynomial
   * @param delta - the computations precision (e.g. what values are considered to be zero / truncated)
   * @param polyRing - the polyring the polynomials are living on. The number of indeterminates
   * has to correspond with Points' number of columns. 
   * @param basis - output, the computed basis of the approximate vanishing ideal
   * @param type - optional, the type of the basis. At the moment only BBasis is supported.
   * @param svdType - Deprecated option as ABM does not use the SVD. 
   * @param memoryType - Deprecated option as ABM does not use the SVD.  
   * @param orderIdeal - optional. if != NULL it will be filled with the orderideal's polynomials.
   * @see ComputeSubIdealOfPointsABM
   * @deprecated
   */
    
  void ComputeIdealOfPointsABM(ABM::DoubleDenseMatrix &points,
                            const double eps, 
                            const double delta,
                            const CoCoA::SparsePolyRing &polyRing, 
                            CoCoA::PolyList &basis,
                            const ABM::BasisType myType=ABM::BBasis,
                            const ABM::SVDType  svdType=ABM::Optimized,
                            const ABM::SVDMemorySize memoryType = ABM::SVDMinimalMemory,
                            CoCoA::PolyList *orderIdeal = NULL,
							bool isNoMoreLinear = false);
							
  /**
  * \brief ComputeIdealOfPointsABM computes approximate vanishing ideals
   *
   *  ComputeIdealOfPointsABM - this method computes a basis for a approximate vanishing
   *  ideal.
   *  There are different versions of this algorithm implemented and various
   *  options, explained below.  
   *
   * @param points - The set of points to compute the apVI on. Each row represents one point, 
   * so the number of columns defines the points' dimension.
   * @param polyRing - the polyring the polynomials are living on. The number of indeterminates
   * has to correspond with Points' number of columns. 
   * @param basis - output, the computed basis of the approximate vanishing ideal
   * @param orderIdeal - optional. if != NULL it will be filled with the orderideal's polynomials.
   * @param options - sets the optional parameters for ABM
   * @see ComputeSubIdealOfPointsABM
   */							
   void ComputeIdealOfPointsABM(ABM::DoubleDenseMatrix &points,
                            const CoCoA::SparsePolyRing &polyRing, 
                            CoCoA::PolyList &basis,
                            CoCoA::PolyList *orderIdeal = NULL,
							const OptionsABM &options = NULL);
							
  /**
   * \brief ComputeSubIdealOfPointsABM computes the intersection of an ideal with an approximate vanishing ideal
   *
   *  ComputeSubIdealOfPointsABM - this method computes a basis for a approximate vanishing
   *  sub ideal.
   *  The generalization allows to compute the apVI inside another ideal.
   *  Also then, the orderIdeal is the layer between the two ideals and it 
   *  is no more monomial, but of the form monomial * Element of GivenBasis.
   *  There are different versions of this algorithm implemented and various
   *  options, explained below.  
   *
   * @param points - The set of points to compute the apVI on. Each row represents one point, 
   * so the number of columns defines the points' dimension.
   * @param eps - the maximum error in a basis polynomial
   * @param delta - the computations precision (e.g. what values are considered to be zero / truncated)
   * @param polyRing - the polyring the polynomials are living on. The number of indeterminates
   * has to correspond with Points' number of columns. 
   * @param basis - output, the computed basis of the approximate vanishing ideal
   * @param givenBasis - the additional parameter. the computed ideal is a subideal of the one, 
   * given by this parameter. It has to be a PolyList in polyRing.
   * @param type - optional, the type of the basis. At the moment only BBasis is supported.
   * @param svdType - Deprecated option as ABM does not use the SVD. 
   * @param memoryType - Deprecated option as ABM does not use the SVD.  
   * @param orderIdeal - optional. if != NULL it will be filled with the orderideal's polynomials.
   * @see ComputeSubIdealOfPointsABM
   * @deprecated
   */
  
  void ComputeSubIdealOfPointsABM(ABM::DoubleDenseMatrix& Points,
                               const double eps, 
                               const double delta,
                               const CoCoA::SparsePolyRing& polyRing, 
                               CoCoA::PolyList &basis,
                               CoCoA::PolyList &givenBasis,
                               const ABM::BasisType type = ABM::BBasis,
                               const ABM::SVDType svdType = ABM::Optimized,
                               const ABM::SVDMemorySize memoryType = ABM::SVDMinimalMemory,
                               CoCoA::PolyList *orderIdeal = NULL,
							   bool isNoMoreLinear = false);

  /**
   * \brief ComputeSubIdealOfPointsABM computes the intersection of an ideal with an approximate vanishing ideal
   *
   *  ComputeSubIdealOfPoints - this method computes a basis for a approximate vanishing
   *  sub ideal.
   *  The generalization allows to compute the apVI inside another ideal.
   *  Also then, the orderIdeal is the layer between the two ideals and it 
   *  is no more monomial, but of the form monomial * Element of GivenBasis.
   *  There are different versions of this algorithm implemented and various
   *  options, explained below.  
   *
   * @param points - The set of points to compute the apVI on. Each row represents one point, 
   * so the number of columns defines the points' dimension.
   * @param polyRing - the polyring the polynomials are living on. The number of indeterminates
   * has to correspond with Points' number of columns. 
   * @param basis - output, the computed basis of the approximate vanishing ideal
   * @param givenBasis - the additional parameter. the computed ideal is a subideal of the one, 
   * given by this parameter. It has to be a PolyList in polyRing.
   * @param orderIdeal - optional. if != NULL it will be filled with the orderideal's polynomials.
   * @param options - sets the optional parameters for ABM
   * @see ComputeSubIdealOfPointsABM
   */							   
   void ComputeSubIdealOfPointsABM(ABM::DoubleDenseMatrix &points,
                               const CoCoA::SparsePolyRing &polyRing, 
                               CoCoA::PolyList &basis,
                               CoCoA::PolyList &givenBasis,
                               CoCoA::PolyList *orderIdeal = NULL,
							   const OptionsABM &options = NULL);
						

 /**
  * \brief ComputeIdealOfPointsExtABM computes approximate vanishing ideals
   *
   *  ComputeIdealOfPointsExtABM - this method computes a basis for an approximate vanishing
   *  ideal. A special property of every polynomial is that every polynomial is a model for 
   *  funValues. To obtain an almost vanishing ideal you have to add an aditional variable.
   *
   * @param points - The set of points to compute the apVI on. Each row represents one point, 
   * so the number of columns defines the points' dimension.
   * @param funValues - The set of points which we want to approximate.
   * @param polyRing - The polyRing the polynomials are living on. The number of indeterminates
   * has to correspond with Points' number of columns. 
   * @param basis - output, the computed basis of the approximate vanishing ideal
   * @param orderIdeal - optional. if != NULL it will be filled with the orderideal's polynomials.
   * @param options - sets the optional parameters for ExtABM
   * @see ComputeSubIdealOfPointsExtABM
   */
    

  void ComputeIdealOfPointsExtABM(ABM::DoubleDenseMatrix &points,
                            ABM::DoubleDenseMatrix &funValues,
                            const CoCoA::SparsePolyRing &polyRing, 
                            CoCoA::PolyList &basis,
                            CoCoA::PolyList *orderIdeal = NULL,
							const OptionsABM &options = NULL);
							
   
  /**
   * \brief ComputeSubIdealOfPoints computes the intersection of an ideal with an approximate vanishing ideal
   *
   *  ComputeSubIdealOfPoints - this method computes a basis for a approximate vanishing
   *  sub ideal.
   *  The generalization allows to compute the apVI inside another ideal.
   *  Also then, the orderIdeal is the layer between the two ideals and it 
   *  is no more monomial, but of the form monomial * Element of givenBasis.
   *  There are different versions of this algorithm implemented and various
   *  options, explained below.  
   *
   * @param points - The set of points to compute the apVI on. Each row represents one point, 
   * so the number of columns defines the points' dimension.
   * @param funValues - The set of points which we want to approximate.
   * @param polyRing - The polyRing the polynomials are living on. The number of indeterminates
   * has to correspond with Points' number of columns. 
   * @param basis - output, the computed basis of the approximate vanishing ideal
   * @param givenBasis - The computed ideal is a subideal of the one, 
   * given by this parameter. It has to be a PolyList in polyRing.
   * @param orderIdeal - optional. if != NULL it will be filled with the orderideal's polynomials.
   * @param options - sets the optional parameters for ExtABM
   * @see ComputeSubIdealOfPointsExtABM

   */
							   
	void ComputeSubIdealOfPointsExtABM(ABM::DoubleDenseMatrix &points,
                               ABM::DoubleDenseMatrix &funValues,
                               const CoCoA::SparsePolyRing &polyRing, 
                               CoCoA::PolyList &basis,
                               CoCoA::PolyList &givenBasis,
                               CoCoA::PolyList *orderIdeal = NULL,
							   const OptionsABM &options = NULL);
							   
/**
  * \brief ComputeIdealOfPointsABM computes an exact border basis
   *
   *  ComputeIdealOfPointsABM - this method computes a basis for a approximate vanishing
   *  ideal.
   *  There are different versions of this algorithm implemented and various
   *  options, explained below.  
   *
   *
   * @param points - The set of points to compute the apVI on. Each row represents one point, 
   * so the number of columns defines the points' dimension. It is an exact matrix.
   * @param eps - has no effect in the exact case
   * @param delta - has no effect in the exact case
   * @param polyRing - the polyring the polynomials are living on. The number of indeterminates
   * has to correspond with Points' number of columns. 
   * @param basis - output, the computed basis of the approximate vanishing ideal
   * @param givenBasis - the additional parameter. the computed ideal is a subideal of the one, 
   * given by this parameter. It has to be a PolyList in polyRing.
   * @param type - optional, the type of the basis. At the moment only BBasis is supported.
   * @param svdType - Deprecated option as ABM does not use the SVD. 
   * @param memoryType - Deprecated option as ABM does not use the SVD.  
   * @param orderIdeal - optional. if != NULL it will be filled with the orderideal's polynomials.
   * @see ComputeSubIdealOfPointsABM
   * @deprecated
   */
							
  void ComputeIdealOfPointsABM(CoCoA::matrix &points,
                            const double eps, 
                            const double delta,
                            const CoCoA::SparsePolyRing &polyRing, 
                            CoCoA::PolyList &basis,
                            const ABM::BasisType myType=ABM::BBasis,
                            const ABM::SVDType  svdType=ABM::Optimized,
                            const ABM::SVDMemorySize memoryType = ABM::SVDMinimalMemory,
                            CoCoA::PolyList *orderIdeal = NULL,
							bool isNoMoreLinear = false);


  /**
   * \brief ComputeSubIdealOfPointsABM computes the intersection of an ideal with an approximate vanishing ideal
   *
   *  ComputeSubIdealOfPointsABM - this method computes an exact basis for a vanishing
   *  sub ideal. 
   *  The generalization allows to compute the apVI inside another ideal.
   *  Also then, the OrderIdeal is the layer between the two ideals and it 
   *  is no more monomial, but of the form monomial * Element of GivenBasis.
   *  There are different versions of this algorithm implemented and various
   *  options, explained below.  
   *
   * @param points - The set of points to compute the apVI on. Each row represents one point, 
   * so the number of columns defines the points' dimension.
   * @param polyRing - The polyRing the polynomials are living on. The number of indeterminates
   * has to correspond with Points' number of columns. 
   * @param basis - output, the computed basis of the approximate vanishing ideal
   * @param givenBasis - The computed ideal is a subideal of the one, 
   * given by this parameter. It has to be a PolyList in polyRing.
   * @param orderIdeal - optional. if != NULL it will be filled with the orderideal's polynomials.
   * @param options - sets the optional parameters for ExtABM
   * @see ComputeSubIdealOfPointsExtABM
   */
							   
  void ComputeSubIdealOfPointsABM(CoCoA::matrix &points,
                               const CoCoA::SparsePolyRing &polyRing, 
                               CoCoA::PolyList &basis,
                               CoCoA::PolyList &givenBasis,
                               CoCoA::PolyList *orderIdeal = NULL,
							   const OptionsABM &options = NULL);
							   
	  



} // end of namespace NumericalAlgorithms

} // end of namespace ApCoCoA

#endif
