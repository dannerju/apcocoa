//
// This file is part of the source of ApCoCoALib, the ApCoCoA Library.
//
//   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
//
//   Author: 2010 Philipp Jovanovic
//
// Visit http://apcocoa.org/ for more information regarding ApCoCoA
// and ApCoCoALib.
// Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
// and known issues.
//
// ApCoCoALib is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License (version 3 or later)
// as published by the Free Software Foundation. A copy of the full
// licence may be found in the file COPYING in this directory.
//
// ApCoCoALib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ApCoCoALib; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#ifndef ApCoCoA_DABMUTILITIES_H
#define ApCoCoA_DABMUTILITIES_H

#include "CoCoA/SparsePolyRing.H"
#include "CoCoA/CoCoA4io.H" // CoCoA::PolyList
#include "ApCoCoA/Numerical/Differential/EvaluatedPolynomialsDABM.H"
namespace ApCoCoA {

namespace NumericalAlgorithms{

	CoCoA::RingElem Differentiate(const CoCoA::RingElem& f);

	CoCoA::RingElem NthDerivation(const CoCoA::RingElem& f, const size_t n);

	size_t getGetBlockDiffIndex(const size_t indetIndex, const size_t diffNum, const size_t numIndets );

	size_t GetNextDiffIndex(const size_t indetIndex, const size_t diffNum);

	bool IsAlmostLinearDependent(ABM::EvaluatedPolynomialsDABM *matrix, const double epsilon);

}}



#endif /* DABMUTILITIES_H_ */
