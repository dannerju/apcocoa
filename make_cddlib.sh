#!/bin/bash

git submodule update --init cddlib

#compile cddlib
cd cddlib
./bootstrap
./configure prefix="$(realpath ../.cdd_lib)" CFLAGS="-I$(realpath ../.gmp_lib/include)  -L$(realpath ../.gmp_lib/lib)"
make -j4
make -j4 install
cd ..

#move header files of cddlib to appropriate folders (name was changed in a recent cddlib version)
mv .cdd_lib/include/cddlib .cdd_lib/include/cdd
