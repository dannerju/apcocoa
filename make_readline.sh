#!/bin/bash

git submodule update --init readline

#compile readline
cd readline
./configure --enable-static --with-curses --prefix=$(realpath ../.readline_lib)
make -j4
make install
cd ..
