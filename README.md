# Compiling CoCoA-Lib with libraries

1. clone repo
2. run `make.sh` to build (almost) all external libraries, CoCoA-Lib with CoCoA-5 and the ApCoCoA server. Those
   can be found under `.cocoa_lib` and `.cocoa5` respectively.
3. run `make_apcocoa_package.sh` to make final package in `.apcocoa` containing CoCoA-5 with all the apcocoa-packages and the ApCoCoA-server.

To test the compiled cocoa5, run `test_cocoa5.sh`.

(There is also a gitlab CI that compiles everything in separate jobs in parallel, tests the compiled cocoa5 on several linux-distros, and creates the apcocoa-package.)

