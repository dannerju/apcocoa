#!/bin/bash
set -e

#compile all external libraries, then CoCoAlib, and extract cocoa5-system
#required apt packages: dh-autoreconf, build-essentials, git, libtool, realpath

#make sure all submomdules are downloaded
git submodule update --init --recursive

echo ""
echo " Compile gmp "
echo ""

#gmp
./make_gmp.sh

echo ""
echo " Compile readline "
echo ""

#readline
./make_readline.sh

echo ""
echo " Compile boost "
echo ""

#boost
./make_boost.sh

echo ""
echo " Compile cdd "
echo ""

#cddlib
./make_cddlib.sh

echo ""
echo " Compile gfan "
echo ""

#compile gfanlib
./make_gfanlib.sh

echo ""
echo " Compile frobby "
echo ""

#frobby
./make_frobby.sh


echo ""
echo " Compile mathsat "
echo ""

#mathsat
./make_mathsat.sh


echo ""
echo " Compile gsl "
echo ""

#gsl
./make_gsl.sh


echo ""
echo " Compile normaliz "
echo ""

#normaliz
./make_normaliz.sh


echo ""
echo " Compile CoCoA-Lib "
echo ""

#cocoalib
./make_cocoa_lib.sh

echo ""
echo " Compile ApCoCoA-Server "
echo ""

#make apcocoaserver
./make_apcocoa_server.sh

echo ""
echo " Create ApCoCoA-Package "
echo ""

#create apcocoa-package
./make_apcocoa_package.sh

