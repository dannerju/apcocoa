#!/bin/bash
set -e
#run cocoa5-tests (on compiled cocoa5)

#create copy of cocoa5 and tests
cp -r .cocoa5 .test_cocoa5
cp -r CoCoALib/src/CoCoA-5/tests .test_cocoa5


cd .test_cocoa5/tests
#run tests as in CoCoALib/src/CoCoA-5/tests/Makefile
TESTS="exsegv.cocoa5 exbugs.cocoa5 bug-EvalTwice.cocoa5 ErrMesg.cocoa5 test-output.cocoa5 quine.cocoa5 AnonFunc.cocoa5 demo-GeMiTo2011.cocoa5 demo-Osaka2015.cocoa5 test-manual.cocoa5 whatiscocoa.cocoa5 primary.cocoa5 lecture-HF1.cocoa5 lecture-HF2.cocoa5 lecture-HF3.cocoa5 lecture-HF4.cocoa5 tut-CoCoLA1.cocoa5 tut-CoCoLA2.cocoa5 tut-CoCoLA3.cocoa5 tut-CoCoLA4.cocoa5 RealRoots.cocoa5 ExtLibNORMALIZ.cocoa5 test-ApproxSolve.cocoa5 test-FactorAlgExt.cocoa5 test-HomomorphismOps.cocoa5 test-PrimaryDecomposition0.cocoa5 test-implicit.cocoa5 test-RingElems.cocoa5 test-toric.cocoa5 tricky-references.cocoa5 TutHokkaido2.cocoa5 TutHokkaido3.cocoa5 TutHokkaido4.cocoa5 TutHokkaido5.cocoa5 SourceAnna.cocoa5 radical0dim.cocoa5 radical.cocoa5 tagging.cocoa5"

./RunTests.sh $TESTS 2> /dev/null  #discard err mesgs from the shell script

cd ../..
/bin/rm -r .test_cocoa5
