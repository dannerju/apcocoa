#!/bin/bash

git submodule update --init Normaliz

#compile normaliz
cd Normaliz
./bootstrap.sh
./configure --prefix=$(realpath ../.normaliz_lib) --enable-shared=no --enable-openmp=no --enable-fast-install=no --disable-libtool-lock --with-gmp=$(realpath ../gmp) DBOOST_ROOT=$(realpath ../boost) DGMP_INCLUDE_DIR=$(realpath ../gmp) DGMPXX_INCLUDE_DIR=$(realpath ../gmp) DGMP_LIBRARIES=$(realpath ../gmp/.libs/libgmp.a) DGMPXX_LIBRARIES=$(realpath ../gmp/.libs/libgmpxx.a) DGMP_STATIC_LIBRARIES=$(realpath ../gmp/.libs/libgmp.a) DGMPXX_STATIC_LIBRARIES=$(realpath ../gmp/.libs/libgmpxx.a) BOOSTFLAGS ="-I$(realpath ../.boost_lib/include) -L$(realpath ../.boost_lib/.libs)"
#flags described at 'http://cocoa.dima.unige.it/cocoalib/doc/html/ExternalLibs-Normaliz.html':
make -j4 install
cd ..


