#!/bin/bash

#required apt packages: lzip
/bin/rm -r gmp*
wget -O gmp.tar.lz https://gmplib.org/download/gmp/gmp-6.2.1.tar.lz
tar --lzip -xf gmp.tar.lz
/bin/rm gmp.tar.lz
mv gmp-* gmp
