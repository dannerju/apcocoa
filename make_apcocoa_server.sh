#!/bin/bash

#make cocoa-lib
#required apt packages: dh-autoreconf, build-essentials, git, libtool, realpath

set -e

cd ApCoCoALib
./configure \
  "--with-boost=$(realpath ../.boost_lib/include)"\
  "--with-libgmp=$(realpath ../.gmp_lib/lib/libgmp.a)"\
  "--with-libgfan=$(realpath ../gfanlib/libgfan.a)"\
  "--with-libcddgmp=$(realpath ../.cdd_lib/lib/libcddgmp.a)"\
  "--with-libmathsat=$(realpath ../MathSAT/lib/libmathsat.a)"\
  "--no-readline"\
  #"--prefix=$(realpath ../.apcocoa_lib)"\
  #"--with-libfrobby=$(realpath ../frobby/bin/libfrobby.a)"\ # NOTE throws errors when compiling apcocoa-server!
  #"--with-libreadline=$(realpath ../.readline_lib/lib/libreadline.a)"\
  #"--with-libgsl=$(realpath ../.gsl_lib/lib/libgsl.a)"\
  #"--with-libnormaliz=$(realpath ../.normaliz_lib/lib/libnormaliz.a)" #TODO cocoa compilation fails!
make library -j4 LDFLAGS="-static"
make server -j4 LDFLAGS="-static"
make check
cd ..

#copy server-executable
mkdir .apcocoa_server
cp ApCoCoALib/src/server/ApCoCoAServer .apcocoa_server
