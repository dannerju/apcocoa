#!/bin/bash

#make cocoa-lib
#required apt packages: dh-autoreconf, build-essentials, git, libtool, realpath

#create dirs for lib
mkdir .cocoa_lib
cd .cocoa_lib
mkdir include
mkdir lib
cd ..
#create dir for cocoa5
mkdir .cocoa5

set -e

cd CoCoALib
./configure \
  "--prefix=$(realpath ../.cocoa_lib)"\
  "--with-boost=$(realpath ../.boost_lib/include)"\
  "--with-libgmp=$(realpath ../.gmp_lib/lib/libgmp.a)"\
  "--with-libgfan=$(realpath ../gfanlib/libgfan.a)"\
  "--with-libcddgmp=$(realpath ../.cdd_lib/lib/libcddgmp.a)"\
  "--with-libfrobby=$(realpath ../frobby/bin/libfrobby.a)"\
  "--with-libmathsat=$(realpath ../MathSAT/lib/libmathsat.a)"\
  "--no-readline"\
  #"--with-libreadline=$(realpath ../.readline_lib/lib/libreadline.a)"\
  #"--with-libgsl=$(realpath ../.gsl_lib/lib/libgsl.a)"\
  #"--with-libnormaliz=$(realpath ../.normaliz_lib/lib/libnormaliz.a)" #TODO cocoa compilation fails!
make library doc -j4 LDFLAGS="-static"
make check -j4
make install -j4
cd src/CoCoA-5
make install -j4 COCOA5_INSTALL_DIR=$(realpath ../../../.cocoa5) LDFLAGS="-static"
cd ..
cd ..
cd ..

#remove absolute link of cocoa5-start-script and move actual folder one up
mv .cocoa5/cocoa-*/* .cocoa5
/bin/rm -r .cocoa5/cocoa-*


